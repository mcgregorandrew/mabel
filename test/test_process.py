'''
Created on May 15, 2020

@author: andrewmcgregor
'''
from main.bh_process import *
import pytest


def make_applicant(override_setting_dict):
    a = {
        APPLICANT_FIRST_NAME: "dummy_first_name", 
        APPLICANT_LAST_NAME: "dummy_last_name", 
        APPLICANT_LANG_PROFILE: "B",
        HR_SENIORITY_YEARS: 0, 
        HR_SITE: "ABC",
        HR_LANG: HR_LANG__ZERO,
        
        APPLICANT_SKILL_RN: False, 
        APPLICANT_SKILL_PCA: False,
        APPLICANT_SKILL_Porter: False,
        APPLICANT_SKILL_RPN: False,  
        APPLICANT_SKILL_RA: False,
        APPLICANT_SKILL_WC: False,
        APPLICANT_SKILL_RN: False,
        APPLICANT_SKILL_MH: False,
        
        APPLICANT_EXT_APPL: False,
        APPLICANT_UNIT_EB2BD: "",
        APPLICANT_UNIT_EB3: "",
        APPLICANT_UNIT_EB4: "",
        APPLICANT_UNIT_EBPal: "",
        APPLICANT_UNIT_EB6: "",
        APPLICANT_UNIT_EBGDH: "",
        APPLICANT_UNIT_EBPDrm: "",
        APPLICANT_UNIT_EBGaot: "",
        APPLICANT_UNIT_EBRes: "",
        APPLICANT_UNIT_SV2N: "",    
        APPLICANT_UNIT_SV2S: "",    
        APPLICANT_UNIT_SV3N: "",    
        APPLICANT_UNIT_SV3S: "",    
        APPLICANT_UNIT_SV4N: "",    
        APPLICANT_UNIT_SV4S: "",    
        APPLICANT_UNIT_SV5N: "",    
        APPLICANT_UNIT_SV5S: "",
        APPLICANT_UNIT_NOPREF: False,

        APPLICANT_TIME_FT: 1,   
        APPLICANT_TIME_PT_1: "",   
        APPLICANT_TIME_PT_2: "",   
        APPLICANT_TIME_PT_3: "",  
        APPLICANT_TIME_PT_4: "",    
        APPLICANT_TIME_PT_5: "", 
        APPLICANT_TIME_PT_6: "",    
        APPLICANT_TIME_PT_7: "",   
        APPLICANT_TIME_PT_8: "", 
        APPLICANT_TIME_PT_9: "",
        APPLICANT_TIME_Stat_NoPref: False,

        APPLICANT_ROT_D:"",   
        APPLICANT_ROT_E:"",    
        APPLICANT_ROT_N:"",    
        APPLICANT_ROT_D_E:"",    
        APPLICANT_ROT_D_N:"",    
        APPLICANT_ROT_WW:"",    
        APPLICANT_ROT_ONA8:1,   
        APPLICANT_ROT_ONA12:"",
        }
    for key in override_setting_dict:
        a[key] = override_setting_dict[key]
    return a


def make_position(override_setting_dict):
    a = {
        POSITION_OCCUPATION: "RN",
        POSITION_SHIFT_TYPE: "Day",
        POSITION_SHIFT_HRS: 8,
        POSITION_FTE:  1.0, 
        POSITION_SCHED_UNIT: "1-SV2 North Nursing",
        POSITION_OCCUPATION: APPLICANT_SKILL_RN,
        POSITION_SKILL_C: APPLICANT_SKILL_RN,
        POSITION_TIME_C: APPLICANT_TIME_FT, 
        POSITION_ROTATION_C: APPLICANT_ROT_D,
        POSITION_LANG_DESIG: "Bilingual",
        POSITION_LANG: "B",
        POSITION_TEMP: ""    
        }
    for key in override_setting_dict:
        a[key] = override_setting_dict[key]
    return a


def test_basic_process():
    # Setup up the input data
    positions = [
        make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD}),
        make_position({POSITION_POSITION_ID: "p2", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD}),
        make_position({POSITION_POSITION_ID: "p3", POSITION_UNIT_C: APPLICANT_UNIT_EB3}),
        make_position({POSITION_POSITION_ID: "p4", POSITION_UNIT_C: APPLICANT_UNIT_EB3})
        ]
    
    applicants = [
        make_applicant({HR_EMPLOYEE_ID: "E001", HR_SENIORITY_YEARS: 22, APPLICANT_SKILL_RN: True, APPLICANT_ROT_D: 1, APPLICANT_TIME_FT: 1,
         APPLICANT_UNIT_EB2BD: 1}),
        make_applicant({HR_EMPLOYEE_ID: "E002", HR_SENIORITY_YEARS: 120, APPLICANT_SKILL_RN: True, APPLICANT_ROT_D: 1, APPLICANT_TIME_FT: 1,
         APPLICANT_UNIT_EB3: 2}),
        make_applicant({HR_EMPLOYEE_ID: "E003", HR_SENIORITY_YEARS: 30, APPLICANT_SKILL_RN: True, APPLICANT_ROT_D: 1, APPLICANT_TIME_FT: 1,
         APPLICANT_UNIT_EB2BD: 4}),
        make_applicant({HR_EMPLOYEE_ID: "E004", HR_SENIORITY_YEARS: 20, APPLICANT_SKILL_RN: False, APPLICANT_ROT_D: 1, APPLICANT_TIME_FT: 1,
         APPLICANT_UNIT_EB2BD: 4})
        ]
    
    # Process the data
    out_list, leftover_position_list, leftover_applicant_list = process_positions2(positions, applicants, [], [INCLUDE_SET_INTERNAL], LANGUAGE_FLEXIBLE, 2)
    
    # Check the output
    assert(len(out_list) == 3)
    assert(out_list[0][OFFER_EMPLOYEE_ID] == "E002")
    assert(len(out_list[0][OFFER_POSITION_LIST]) == 2)
    assert(out_list[0][OFFER_CLEAR_TO_OFFER] == True)
    assert(out_list[0][OFFER_POSITION_LIST][0][OFFER_POSITION_LIST_POSITION][POSITION_POSITION_ID] == "p3")
    assert(out_list[0][OFFER_POSITION_LIST][1][OFFER_POSITION_LIST_POSITION][POSITION_POSITION_ID] == "p4")

    assert(out_list[1][OFFER_EMPLOYEE_ID] == "E003")
    assert(len(out_list[1][OFFER_POSITION_LIST]) == 2)
    assert(out_list[1][OFFER_CLEAR_TO_OFFER] == True)
    assert(out_list[1][OFFER_POSITION_LIST][0][OFFER_POSITION_LIST_POSITION][POSITION_POSITION_ID] == "p1")
    assert(out_list[1][OFFER_POSITION_LIST][1][OFFER_POSITION_LIST_POSITION][POSITION_POSITION_ID] == "p2")

    assert(out_list[2][OFFER_EMPLOYEE_ID] == "E001")
    assert(len(out_list[2][OFFER_POSITION_LIST]) == 2)
    assert(out_list[2][OFFER_CLEAR_TO_OFFER] == False)
    assert(out_list[2][OFFER_POSITION_LIST][0][OFFER_POSITION_LIST_POSITION][POSITION_POSITION_ID] == "p1")
    assert(out_list[2][OFFER_POSITION_LIST][1][OFFER_POSITION_LIST_POSITION][POSITION_POSITION_ID] == "p2")

    assert(len(leftover_position_list) == 0)
    assert(leftover_applicant_list[0][HR_EMPLOYEE_ID] == "E004")


def test_any_unit():
    # Check that unit mismatch rejects job
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "B"})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "B", HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, APPLICANT_ROT_D: 1})
    assert(score_position(appl, position, LANGUAGE_FLEXIBLE, 2) == -1)
    
    # Test that "any unit" matches
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "B"})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "B", HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, APPLICANT_UNIT_NOPREF: True, APPLICANT_ROT_D: 1})
    assert(score_position(appl, position, LANGUAGE_FLEXIBLE, 2) > 0)
    


def test_8_12_hour_shifts():
    # Check that shift mismatch rejects job
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "B", POSITION_SHIFT_HRS: 8})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "B", APPLICANT_UNIT_EB2BD: 1, HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, APPLICANT_ROT_D: 1, APPLICANT_ROT_ONA8: 1})
    assert(score_position(appl, position, LANGUAGE_FLEXIBLE, 2) > 0)
    
    # Check that shift mismatch rejects job
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "B", POSITION_SHIFT_HRS: 12})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "B", APPLICANT_UNIT_EB2BD: 1, HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, APPLICANT_ROT_D: 1, APPLICANT_ROT_ONA8: "", APPLICANT_ROT_ONA12: 1})
    assert(score_position(appl, position, LANGUAGE_FLEXIBLE, 2) > 0)
    
    # Check that shift mismatch rejects job
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "B", POSITION_SHIFT_HRS: 8})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "B", APPLICANT_UNIT_EB2BD: 1, HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, APPLICANT_ROT_D: 1, APPLICANT_ROT_ONA8: "", APPLICANT_ROT_ONA12: 1})
    assert(score_position(appl, position, LANGUAGE_FLEXIBLE, 2) == -1)
    
    # Check that 99 shifts are rejected
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "B", POSITION_SHIFT_HRS: 12})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "B", APPLICANT_UNIT_EB2BD: 1, HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, APPLICANT_ROT_D: 1, APPLICANT_ROT_ONA8: 99, APPLICANT_ROT_ONA12: 99})
    assert(score_position(appl, position, LANGUAGE_FLEXIBLE, 2) == -1)
    
    # Check that any shift works
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "B", POSITION_SHIFT_HRS: 8})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "B", APPLICANT_UNIT_EB2BD: 1, HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, APPLICANT_ROT_D: 1, APPLICANT_ROT_ONA8: 2, APPLICANT_ROT_ONA12: 1})
    assert(score_position(appl, position, LANGUAGE_FLEXIBLE, 2) > 0)
        



def test_applicant_data_quality_checks():
    applicants = [
        make_applicant({HR_EMPLOYEE_ID: "e1", HR_SENIORITY_YEARS: 22}),
        make_applicant({HR_EMPLOYEE_ID: "e2", HR_SENIORITY_YEARS: 22}),
        make_applicant({HR_EMPLOYEE_ID: "e3", HR_SENIORITY_YEARS: 22}),
        ]
    for rot in APPLICANT_ROTATION_LIST:
        assert(applicants[0][rot] == '')
        
    applicant_data_quality_check(applicants, RUN_TYPE_CLINICAL, 2)
    
    # Test that if no rotations are specified then all rotations are considered equally
    for rot in APPLICANT_ROTATION_LIST:
        assert(applicants[0][rot] == 1)

    applicants = [
        make_applicant({HR_EMPLOYEE_ID: "e1", HR_SENIORITY_YEARS: 22}),
        make_applicant({HR_EMPLOYEE_ID: "e2", HR_SENIORITY_YEARS: 22}),
        make_applicant({HR_EMPLOYEE_ID: "e1", HR_SENIORITY_YEARS: 22}),
        ]
    try:
        applicant_data_quality_check(applicants, RUN_TYPE_CLINICAL, 2)
        pytest.fail("Exception expected due to duplicate employee numbers")
    except BhException:
        pass
        
    
def test_position_data_quality_checks():
    positions = [
        make_position({POSITION_POSITION_ID: "p1"}),
        make_position({POSITION_POSITION_ID: "p2"}),
        make_position({POSITION_POSITION_ID: "p3"}),
        make_position({POSITION_POSITION_ID: "p4"})
        ]
    position_data_quality_check(positions, 2)
    
    # Check duplicate positions detected
    positions = [
        make_position({POSITION_POSITION_ID: "p1"}),
        make_position({POSITION_POSITION_ID: "p2"}),
        make_position({POSITION_POSITION_ID: "p3"}),
        make_position({POSITION_POSITION_ID: "p1"})
        ]
    try:
        position_data_quality_check(positions, 2)
        pytest.fail("Exception expected")
    except BhException:
        pass
        
    # Check bad occupation
    positions = [
        make_position({POSITION_POSITION_ID: "p1", POSITION_OCCUPATION: "xxx"}),
        ]
    try:
        position_data_quality_check(positions, 2)
        pytest.fail("Exception expected")
    except BhException:
        pass
        
    # Check bad FTE
    positions = [
        make_position({POSITION_POSITION_ID: "p1", POSITION_FTE: 99}),
        ]
    try:
        position_data_quality_check(positions, 2)
        pytest.fail("Exception expected")
    except BhException:
        pass
        
def test_result_close():
    # Setup up the input data
    positions = [
        make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD}),
        make_position({POSITION_POSITION_ID: "p2", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD}),
        ]
    
    applicants = [
        make_applicant({HR_EMPLOYEE_ID: "E001", HR_SENIORITY_YEARS: 22, APPLICANT_SKILL_RN: True, APPLICANT_ROT_D: 1, APPLICANT_TIME_FT: 1,
         APPLICANT_UNIT_EB2BD: 1}),
        make_applicant({HR_EMPLOYEE_ID: "E002", HR_SENIORITY_YEARS: 44, APPLICANT_SKILL_RN: True, APPLICANT_ROT_D: 1, APPLICANT_TIME_FT: 1,
         APPLICANT_UNIT_EB2BD: 1}),
        ]
    
    # Process the data
    out_list, leftover_position_list, leftover_applicant_list = process_positions2(positions, applicants, [], [INCLUDE_SET_INTERNAL], LANGUAGE_FLEXIBLE, 2)
    
    # Check the output
    assert(len(out_list) == 2)
    assert(len(leftover_applicant_list) == 0)
    assert(len(leftover_position_list) == 0)

    # Rerun with employee having rejected p1
    out_list, leftover_position_list, leftover_applicant_list = process_positions2(positions, applicants, 
                                                                                  [{REJ_EMPOYEE_ID: "E001", REJ_POSITION_ID: "", REJ_ACTION:REJ_ACTION__CLOSED}], 
                                                                                  [INCLUDE_SET_INTERNAL], LANGUAGE_FLEXIBLE, 2)
    
    # Check the output
    assert(len(out_list) == 1)
    assert(len(leftover_position_list) == 0)
    assert(len(leftover_applicant_list) == 0)

    
    # Rerun with employee having rejected p1 and p2
    out_list, leftover_position_list, leftover_applicant_list = process_positions2(positions, applicants, 
                                                                                  [{REJ_EMPOYEE_ID: "E001", REJ_POSITION_ID: "", REJ_ACTION:REJ_ACTION__CLOSED},
                                                                                   {REJ_EMPOYEE_ID: "E002", REJ_POSITION_ID: "", REJ_ACTION:REJ_ACTION__CLOSED}], 
                                                                                  [INCLUDE_SET_INTERNAL], LANGUAGE_FLEXIBLE, 2)
    
    # Check the output
    assert(len(out_list) == 0)
    assert(len(leftover_position_list) == 2)
    assert(len(leftover_applicant_list) == 0)

    
def test_result_acceptance():
    # Setup up the input data
    positions = [
        make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD}),
        make_position({POSITION_POSITION_ID: "p2", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD}),
        ]
    
    applicants = [
        make_applicant({HR_EMPLOYEE_ID: "E001", HR_SENIORITY_YEARS: 22, APPLICANT_SKILL_RN: True, APPLICANT_ROT_D: 1, APPLICANT_TIME_FT: 1,
         APPLICANT_UNIT_EB2BD: 1}),
        make_applicant({HR_EMPLOYEE_ID: "E002", HR_SENIORITY_YEARS: 11, APPLICANT_SKILL_RN: True, APPLICANT_ROT_D: 1, APPLICANT_TIME_FT: 1,
         APPLICANT_UNIT_EB2BD: 1}),
        ]
    
    # Process the data
    out_list, leftover_position_list, leftover_applicant_list = process_positions2(positions, applicants, [], [INCLUDE_SET_INTERNAL], LANGUAGE_FLEXIBLE, 2)
    
    # Check the output
    assert(len(out_list) == 2)
    assert(len(leftover_position_list) == 0)
    assert(len(leftover_applicant_list) == 0)

    # Rerun with employee having accepted p1 with a score of 200.  P2 is 4 or it is offered to E001
    out_list, leftover_position_list, leftover_applicant_list = process_positions2(positions, applicants, 
                                                                                  [{REJ_EMPOYEE_ID: "E001", REJ_POSITION_ID: "p1", REJ_ACTION:REJ_ACTION__ACCEPTED,
                                                                                    REJ_SCORE: 200}],
                                                                                  [INCLUDE_SET_INTERNAL], LANGUAGE_FLEXIBLE, 
                                                                                  2)
    
    # Check the output
    assert(len(out_list) == 2)
    assert(len(leftover_position_list) == 0)
    assert(len(leftover_applicant_list) == 0)

    
    # Rerun with employee having accepted p1 with a score of 2.  P2 is 4 or it is not offered to E001
    out_list, leftover_position_list, leftover_applicant_list = process_positions2(positions, applicants, 
                                                                                  [{REJ_EMPOYEE_ID: "E001", REJ_POSITION_ID: "p1", REJ_ACTION:REJ_ACTION__ACCEPTED,
                                                                                    REJ_SCORE: 2}],
                                                                                  [INCLUDE_SET_INTERNAL], LANGUAGE_FLEXIBLE, 
                                                                                  2)
    
    # Check the output
    assert(len(out_list) == 1)
    assert(len(leftover_position_list) == 0)
    assert(len(leftover_applicant_list) == 0)

    
def test_result_rejected():
    # Setup up the input data
    positions = [
        make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD}),
        make_position({POSITION_POSITION_ID: "p2", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD}),
        ]
    
    applicants = [
        make_applicant({HR_EMPLOYEE_ID: "E001", HR_SENIORITY_YEARS: 22, APPLICANT_SKILL_RN: True, APPLICANT_ROT_D: 1, APPLICANT_TIME_FT: 1,
         APPLICANT_UNIT_EB2BD: 1}),
        make_applicant({HR_EMPLOYEE_ID: "E002", HR_SENIORITY_YEARS: 11, APPLICANT_SKILL_RN: True, APPLICANT_ROT_D: 1, APPLICANT_TIME_FT: 1,
         APPLICANT_UNIT_EB2BD: 1}),
        ]
    
    # Process the data
    out_list, leftover_position_list, leftover_applicant_list = process_positions2(positions, applicants, [], [INCLUDE_SET_INTERNAL], LANGUAGE_FLEXIBLE, 2)
    
    # Check the output - 2 applicants each with 2 offers
    assert(len(out_list) == 2)
    assert(len(out_list[0][OFFER_POSITION_LIST]) == 2)
    assert(len(out_list[1][OFFER_POSITION_LIST]) == 2)
    assert(len(leftover_position_list) == 0)
    assert(len(leftover_applicant_list) == 0)

    # Rerun with employee having been rejected for p1
    out_list, leftover_position_list, leftover_applicant_list = process_positions2(positions, applicants, 
                                                                                  [{REJ_EMPOYEE_ID: "E001", REJ_POSITION_ID: "p1", REJ_ACTION:REJ_ACTION__REJECTED}],
                                                                                  [INCLUDE_SET_INTERNAL], LANGUAGE_FLEXIBLE, 
                                                                                  2)
    
    # Check the output
    assert(len(out_list) == 2)
    assert(len(out_list[0][OFFER_POSITION_LIST]) == 1)
    assert(len(out_list[1][OFFER_POSITION_LIST]) == 2)
    assert(len(leftover_position_list) == 0)
    assert(len(leftover_applicant_list) == 0)

    # Rerun with employee having been rejected for p1
    out_list, leftover_position_list, leftover_applicant_list = process_positions2(positions, applicants, 
                                                                                  [{REJ_EMPOYEE_ID: "E001", REJ_POSITION_ID: "p1", REJ_ACTION:REJ_ACTION__REJECTED},
                                                                                   {REJ_EMPOYEE_ID: "E001", REJ_POSITION_ID: "p2", REJ_ACTION:REJ_ACTION__REJECTED}],
                                                                                  [INCLUDE_SET_INTERNAL], LANGUAGE_FLEXIBLE, 
                                                                                  2)
    
    # Check the output
    assert(len(out_list) == 1)
    assert(len(out_list[0][OFFER_POSITION_LIST]) == 2)
    assert(len(leftover_position_list) == 0)
    assert(len(leftover_applicant_list) == 1)

    
def test_internal_vs_external():
    # Setup up the input data
    positions = [
        make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD}),
        make_position({POSITION_POSITION_ID: "p2", POSITION_UNIT_C: APPLICANT_UNIT_EB3}),
        ]
    
    applicants = [
        # External applicant
        make_applicant({APPLICANT_FIRST_NAME: "fn1", APPLICANT_LAST_NAME: "ln1", APPLICANT_EMAIL: "e1", 
                        APPLICANT_SKILL_RN: True, APPLICANT_ROT_D: 1, APPLICANT_TIME_FT: 1,
                        APPLICANT_UNIT_EB2BD: 1, APPLICANT_UNIT_EB3: 2, APPLICANT_EXT_APPL: True}),
        # Internal applicant
        make_applicant({APPLICANT_FIRST_NAME: "fn2", APPLICANT_LAST_NAME: "ln2", APPLICANT_EMAIL: "e2", 
                        APPLICANT_SKILL_RN: True, APPLICANT_ROT_D: 1, APPLICANT_TIME_FT: 1,
                        APPLICANT_UNIT_EB2BD: 1, APPLICANT_UNIT_EB3: 2, APPLICANT_EXT_APPL: False}),
        # Residence St Louis applicant
        make_applicant({APPLICANT_FIRST_NAME: "fn3", APPLICANT_LAST_NAME: "ln3", APPLICANT_EMAIL: "e3", 
                        APPLICANT_SKILL_RN: True, APPLICANT_ROT_D: 1, APPLICANT_TIME_FT: 1,
                        APPLICANT_UNIT_EB2BD: 1, APPLICANT_UNIT_EB3: 2, APPLICANT_EXT_APPL: False, HR_SITE: HR_SITE__RESIDENCE_ST_LOUIS})
        ]

    hr_list = [
        {HR_FIRST_NAME: "fn1", HR_LAST_NAME: "ln1", HR_EMAIL_ADDRESS: "e1", HR_EMPLOYEE_ID: ""},
        {HR_FIRST_NAME: "fn2", HR_LAST_NAME: "ln2", HR_EMAIL_ADDRESS: "e2", HR_EMPLOYEE_ID: "E002", HR_SENIORITY_YEARS: 100},
        # All RSL staff have 0 hours seniority for hospital positions
        {HR_FIRST_NAME: "fn3", HR_LAST_NAME: "ln3", HR_EMAIL_ADDRESS: "e3", HR_EMPLOYEE_ID: "E003", HR_SENIORITY_YEARS: 0}
        ]
    new_applicants = merge_hr_into_applicants(applicants, hr_list, 
                                              RUN_TYPE_CLINICAL,  # Clinical 
                                              2)     # Verbose
    # Process the data with internal only
    out_list, _, _ = process_positions2(positions, new_applicants, [], [INCLUDE_SET_INTERNAL], LANGUAGE_FLEXIBLE, 2)
    
    # Check the output
    assert(len(out_list) == 1)
    appl = out_list[0][OFFER_APPLICANT]
    assert(appl[HR_EMPLOYEE_ID] == "E002")

    # Rerun with list internal and RSL
    out_list, _, _ = process_positions2(positions, new_applicants, [],                                                                                  
                                        [INCLUDE_SET_INTERNAL, INCLUDE_SET_RSL], LANGUAGE_FLEXIBLE, 2)
    
    # Check the output
    assert(len(out_list) == 2)
    appl = out_list[0][OFFER_APPLICANT]
    assert(appl[HR_EMPLOYEE_ID] == "E002")
    appl = out_list[1][OFFER_APPLICANT]
    assert(appl[HR_EMPLOYEE_ID] == "E003")


    
    # Rerun with list internal, RSL and external
    out_list, _, _ = process_positions2(positions, new_applicants, [],                                                                                  
                                        [INCLUDE_SET_INTERNAL, INCLUDE_SET_RSL, INCLUDE_SET_EXTERNAL], LANGUAGE_FLEXIBLE, 2)
    
    # Check the output
    assert(len(out_list) == 3)
    appl = out_list[0][OFFER_APPLICANT]
    assert(appl[HR_EMPLOYEE_ID] == "E002")
    appl = out_list[1][OFFER_APPLICANT]
    assert("EXTAPP" in appl[HR_EMPLOYEE_ID])
    appl = out_list[2][OFFER_APPLICANT]
    assert(appl[HR_EMPLOYEE_ID] == "E003")

    
def test_result_data_quality_check():
    positions = [
        make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD})
        ]
    
    applicants = [
        make_applicant({HR_EMPLOYEE_ID: "E1", APPLICANT_FIRST_NAME: "fn1", APPLICANT_LAST_NAME: "ln1", APPLICANT_EMAIL: "a@a.com", HR_SENIORITY_YEARS: 22, APPLICANT_SKILL_RN: True, APPLICANT_ROT_D: 1, APPLICANT_TIME_FT: 1,
         APPLICANT_UNIT_EB2BD: 1})
        ]

    results = [{REJ_FIRST_NAME: "fn1", REJ_LAST_NAME: "ln1", REJ_EMAIL: "a@a.com", REJ_EMPOYEE_ID: "E1", REJ_POSITION_ID: "p1", REJ_FTE: 1, REJ_ROTATION: "Day", REJ_ACTION: REJ_ACTION__ACCEPTED}]
    result_data_quality_check(results, applicants, positions)
    
    # Try with wrong last name
    results = [{REJ_FIRST_NAME: "fn1", REJ_LAST_NAME: "ln2", REJ_EMAIL: "a@a.com", REJ_EMPOYEE_ID: "E1", REJ_POSITION_ID: "p1", REJ_FTE: 1, REJ_ROTATION: "Day", REJ_ACTION: REJ_ACTION__ACCEPTED}]
    try:
        result_data_quality_check(results, applicants, positions)
        pytest.fail("Exception expected")
    except BhException:
        pass
    
    # Try with wrong FTE value
    results = [{REJ_FIRST_NAME: "fn1", REJ_LAST_NAME: "ln1", REJ_EMAIL: "a@a.com", REJ_EMPOYEE_ID: "E1", REJ_POSITION_ID: "p1", REJ_FTE: 0.11, REJ_ROTATION: "D", REJ_ACTION: REJ_ACTION__ACCEPTED}]
    try:
        result_data_quality_check(results, applicants, positions)
        pytest.fail("Exception expected")
    except BhException:
        pass
    
    
def test_langauge_flexible():
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "B"})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "B", HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, APPLICANT_UNIT_EB2BD: 1, APPLICANT_ROT_D: 1})
    assert(500 > score_position(appl, position, LANGUAGE_FLEXIBLE, 2) > 0)
    
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "B"})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "E", HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, APPLICANT_UNIT_EB2BD: 1, APPLICANT_ROT_D: 1})
    assert(score_position(appl, position, LANGUAGE_FLEXIBLE, 2) > 1000)
    
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "B"})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "F", HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, APPLICANT_UNIT_EB2BD: 1, APPLICANT_ROT_D: 1})
    assert(score_position(appl, position, LANGUAGE_FLEXIBLE, 2) > 1000)
    
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "E"})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "B", HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, APPLICANT_UNIT_EB2BD: 1, APPLICANT_ROT_D: 1})
    assert(500 > score_position(appl, position, LANGUAGE_FLEXIBLE, 2) > 0)
    
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "E"})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "E", HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, APPLICANT_UNIT_EB2BD: 1, APPLICANT_ROT_D: 1})
    assert(500 > score_position(appl, position, LANGUAGE_FLEXIBLE, 2) > 0)
    
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "E"})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "F", HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, APPLICANT_UNIT_EB2BD: 1, APPLICANT_ROT_D: 1})
    assert(score_position(appl, position, LANGUAGE_FLEXIBLE, 2) > 1000)
    
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "F"})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "B", HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, APPLICANT_UNIT_EB2BD: 1, APPLICANT_ROT_D: 1})
    assert(500 > score_position(appl, position, LANGUAGE_FLEXIBLE, 2) > 0)
    
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "F"})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "E", HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, APPLICANT_UNIT_EB2BD: 1, APPLICANT_ROT_D: 1})
    assert(score_position(appl, position, LANGUAGE_FLEXIBLE, 2) > 1000)
    
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "F"})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "F", HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, APPLICANT_UNIT_EB2BD: 1, APPLICANT_ROT_D: 1})
    assert(500 > score_position(appl, position, LANGUAGE_FLEXIBLE, 2) > 0)
    
    
def test_langauge_strict():
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "B"})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "B", HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, APPLICANT_UNIT_EB2BD: 1, APPLICANT_ROT_D: 1})
    assert(500 > score_position(appl, position, LANGUAGE_STRICT, 2) > 0)
    
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "B"})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "E", HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, APPLICANT_UNIT_EB2BD: 1, APPLICANT_ROT_D: 1})
    assert(score_position(appl, position, LANGUAGE_STRICT, 2) == -1)
    
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "B"})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "F", HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, APPLICANT_UNIT_EB2BD: 1, APPLICANT_ROT_D: 1})
    assert(score_position(appl, position, LANGUAGE_STRICT, 2) == -1)
    
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "E"})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "B", HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, APPLICANT_UNIT_EB2BD: 1, APPLICANT_ROT_D: 1})
    assert(500 > score_position(appl, position, LANGUAGE_STRICT, 2) > 0)
    
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "E"})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "E", HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, APPLICANT_UNIT_EB2BD: 1, APPLICANT_ROT_D: 1})
    assert(500 > score_position(appl, position, LANGUAGE_STRICT, 2) > 0)
    
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "E"})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "F", HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, APPLICANT_UNIT_EB2BD: 1, APPLICANT_ROT_D: 1})
    assert(score_position(appl, position, LANGUAGE_STRICT, 2) == -1)
    
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "F"})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "B", HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, APPLICANT_UNIT_EB2BD: 1, APPLICANT_ROT_D: 1})
    assert(500 > score_position(appl, position, LANGUAGE_STRICT, 2) > 0)
    
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "F"})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "E", HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, APPLICANT_UNIT_EB2BD: 1, APPLICANT_ROT_D: 1})
    assert(score_position(appl, position, LANGUAGE_STRICT, 2) == -1)
    
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "F"})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "F", HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, APPLICANT_UNIT_EB2BD: 1, APPLICANT_ROT_D: 1})
    assert(500 > score_position(appl, position, LANGUAGE_STRICT, 2) > 0)
    
    
def test_langauge_hr():
    '''
    We need to test that we use the QHR language if it is available, otherwise we use the self-declared language
    '''
    
    # Test that QHR language is used if available
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "B"})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "B", HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, 
                           APPLICANT_UNIT_EB2BD: 1, APPLICANT_ROT_D: 1,
                           HR_LANG: HR_LANG__ENGLISH})
    assert(score_position(appl, position, LANGUAGE_STRICT, 2) == -1)
    
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "B"})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "E", HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, 
                           APPLICANT_UNIT_EB2BD: 1, APPLICANT_ROT_D: 1,
                           HR_LANG: HR_LANG__BILINGUAL})
    assert(500 > score_position(appl, position, LANGUAGE_STRICT, 2) > 0)
    
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "B"})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "E", HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, 
                           APPLICANT_UNIT_EB2BD: 1, APPLICANT_ROT_D: 1})
    assert(score_position(appl, position, LANGUAGE_STRICT, 2) == -1)
    
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "B"})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "B", HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, 
                           APPLICANT_UNIT_EB2BD: 1, APPLICANT_ROT_D: 1})
    assert(500 > score_position(appl, position, LANGUAGE_STRICT, 2) > 0)
    
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "B"})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "E", HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, 
                           APPLICANT_UNIT_EB2BD: 1, APPLICANT_ROT_D: 1})
    assert(score_position(appl, position, LANGUAGE_STRICT, 2) == -1)
    
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "B"})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "B", HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, 
                           APPLICANT_UNIT_EB2BD: 1, APPLICANT_ROT_D: 1,
                           HR_LANG: HR_LANG__UNKNOWN})
    assert(500 > score_position(appl, position, LANGUAGE_STRICT, 2) > 0)
    
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "B"})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "E", HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, 
                           APPLICANT_UNIT_EB2BD: 1, APPLICANT_ROT_D: 1,
                           HR_LANG: HR_LANG__NAN})
    assert(score_position(appl, position, LANGUAGE_STRICT, 2) == -1)
    
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "B"})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "B", HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, 
                           APPLICANT_UNIT_EB2BD: 1, APPLICANT_ROT_D: 1,
                           HR_LANG: HR_LANG__ZERO})
    assert(500 > score_position(appl, position, LANGUAGE_STRICT, 2) > 0)
    
    
    
def test_time_matching():
    # Applicant wants FTE but position is 0.9.  No match.
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "B", POSITION_FTE: 0.9, POSITION_TIME_C: "PT.9"})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "B", HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, APPLICANT_UNIT_EB2BD: 1, APPLICANT_ROT_D: 1})
    assert(score_position(appl, position, LANGUAGE_STRICT, 2) == -1)
    
    # Applicant wants FTE or 0.9 but position is 0.9.  Match.
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "B", POSITION_FTE: 0.9, POSITION_TIME_C: "PT.9"})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "B", HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, APPLICANT_UNIT_EB2BD: 1, APPLICANT_ROT_D: 1, 
                           APPLICANT_TIME_PT_9: 2})
    assert(score_position(appl, position, LANGUAGE_STRICT, 2) > 0)
    
    # Applicant will take any time.  Match is OK for 0.9 and 0.1
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "B", POSITION_FTE: 0.9, POSITION_TIME_C: "PT.9"})
    appl = make_applicant({APPLICANT_LANG_PROFILE: "B", HR_EMPLOYEE_ID: "E1", APPLICANT_SKILL_RN: True, APPLICANT_UNIT_EB2BD: 1, APPLICANT_ROT_D: 1,
                           APPLICANT_TIME_FT: "", APPLICANT_TIME_Stat_NoPref: True})
    assert(score_position(appl, position, LANGUAGE_STRICT, 2) > 0)
    
    
def test_fractional_fte():
    assert(round_up_to_nearest_tenth(0.1) == 0.1)
    assert(round_up_to_nearest_tenth(0.15) == 0.2)
    assert(round_up_to_nearest_tenth(0.480) == 0.5)
    
    position = make_position({POSITION_POSITION_ID: "p1", POSITION_UNIT_C: APPLICANT_UNIT_EB2BD, POSITION_LANG: "B", POSITION_FTE: 0.81})
    position_data_quality_check([position], 2)
    assert(position[POSITION_TIME_C] == "PT.9")