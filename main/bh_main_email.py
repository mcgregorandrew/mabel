#!/usr/local/bin/python2.7
# encoding: utf-8
'''
main.bh_main_email -- Bruyere program to generate email with preferences to allow users to confirm their preferences.


@author:     Andrew McGregor

@copyright:  2020 Andrew McGregor. All rights reserved.

@license:    None

@contact:    mcgregorandrew@yahoo.ca
@deffield    updated: Updated
'''

import os 

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter
from main.bh_process import *  # @UnusedWildImport
import traceback

__all__ = []
__version__ = 0.1
__date__ = '2020-05-15'
__updated__ = '2020-05-15'


EMAIL_ADDRESS="Email"
EMAIL_TEXT="EmailText"

class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''
    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg
    def __str__(self):
        return self.msg
    def __unicode__(self):
        return self.msg


def make_prefs(applicant, pref_list, mapper_dict, any_field):
    if any_field is not None and applicant[any_field]:
        return "any"
    
    rec_list = []
    for field in pref_list:
        if not math.isnan(applicant[field]) and applicant[field] != 99:
            rec_list.append({"PREF": applicant[field], "FIELD": mapper_dict[field]})
    # Sort the list
    unit_pref_list = sorted(rec_list, key = lambda i: (i["PREF"]))
    # Output string
    mini_list = [s["FIELD"] for s in unit_pref_list]
    return ','.join(mini_list)


unit_mapper = {APPLICANT_UNIT_EB2BD: "EB 2nd floor",
                                APPLICANT_UNIT_EB3: "EB 3rd floor",
                                APPLICANT_UNIT_EB4: "EB cxxx",
                                APPLICANT_UNIT_EBPal: "EB cxxx",
                                APPLICANT_UNIT_EB6: "EB cxxx",
                                APPLICANT_UNIT_EBGDH: "EB cxxx",
                                APPLICANT_UNIT_EBPDrm: "EB cxxx",
                                APPLICANT_UNIT_EBGaot: "EB cxxx",
                                APPLICANT_UNIT_EBRes: "EB cxxx",
                                APPLICANT_UNIT_SV2N: "SV cxxx",   
                                APPLICANT_UNIT_SV2S: "SV cxxx",    
                                APPLICANT_UNIT_SV3N: "SV cxxx", 
                                APPLICANT_UNIT_SV3S: "SV cxxx", 
                                APPLICANT_UNIT_SV4N: "SV cxxx",
                                APPLICANT_UNIT_SV4S: "SV cxxx", 
                                APPLICANT_UNIT_SV5N: "SV cxxx",
                                APPLICANT_UNIT_SV5S: "SV cxxx"}

fte_mapper_dict = {APPLICANT_TIME_FT: "Full-time",    
                   APPLICANT_TIME_PT_1:"0.1 time",   
                   APPLICANT_TIME_PT_2:"0.2 time", 
                   APPLICANT_TIME_PT_3:"0.3 time",
                   APPLICANT_TIME_PT_4:"0.4 time", 
                   APPLICANT_TIME_PT_5:"0.5 time",
                   APPLICANT_TIME_PT_6:"0.6 time",
                   APPLICANT_TIME_PT_7:"0.7 time",
                   APPLICANT_TIME_PT_8:"0.8 time",
                   APPLICANT_TIME_PT_9:"0.9 time"}

rot_mapper_dict = {APPLICANT_ROT_D: "Days",    
                   APPLICANT_ROT_E: "Evenings",
                   APPLICANT_ROT_N: "Nights",    
                   APPLICANT_ROT_D_E: "Days/Evenings",    
                   APPLICANT_ROT_D_N: "Nights",    
                   APPLICANT_ROT_WW: "Weekend Work"}

ona_mapper_dict = {APPLICANT_ROT_ONA8: "8 hour shifts", APPLICANT_ROT_ONA12: "12 hour shifts"}

def make_email_text(applicant, clinical, verbose):
    string_list = []
    string_list.append("Thank you for your application for positions at Bruyère.")
    string_list.append("Shown below is what we recorded as your preferences in order of preference.  Please let us know if this is not correct, within 24 hours.")
    string_list.append("")
    if clinical:
        unit_list = APPLICANT_UNIT_LIST_CLINICAL
        occupation_list = [occ for occ in CLINICAL_OCCUPATIONS]
    else:
        unit_list = APPLICANT_UNIT_LIST_NON_CLINICAL
        occupation_list = [occ for occ in NONCLINICAL_OCCUPATIONS]
        
    mini_list = [occ for occ in occupation_list if applicant[occ]]
    string_list.append(f"Occupations: {','.join(mini_list)}")
    
    # Make string for unit preferences
    string_list.append(f"Unit Preferences: {make_prefs(applicant, unit_list, unit_mapper, APPLICANT_UNIT_NOPREF)}")
    
    # Make string for fte preferences
    string_list.append(f"Time Preferences: {make_prefs(applicant, APPLICANT_FTE_LIST, fte_mapper_dict, APPLICANT_TIME_Stat_NoPref)}")
    
    # Make string for rotation preferences
    string_list.append(f"Rotation Preferences: {make_prefs(applicant, APPLICANT_ROTATION_LIST, rot_mapper_dict, None)}")
    
    
    # Make string for shift length preferences
    if applicant[APPLICANT_SKILL_RN]:
        string_list.append(f"Shift Length Preferences: {make_prefs(applicant, APPLICANT_SHIFT_LENGTH_LIST, ona_mapper_dict, None)}")
    
    # Make text
    return "\n".join(string_list)


def main(argv=None): # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    program_license = '''%s

  Created by Andrew McGregor on %s.
  Copyright 2020 Andrew McGregor. All rights reserved.

  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc, str(__date__))

    try:
        # Setup argument parser
        parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
        parser.add_argument("-a", "--applicants", help="CSV file of applicants with preferences", required=True)
        parser.add_argument("-l", "--list_of_emails", help="Store all files in top level folder", required=True)
        parser.add_argument("-o", "--output_dir", help="Folder to write output files to", required=True)
        parser.add_argument("-t", "--type", help="Type of files, c for clinical or n for non-clinical", required=True)
        parser.add_argument('-V', '--version', action='version', version=program_version_message)
        parser.add_argument("-v", "--verbose", dest="verbose", action="count", help="set verbosity level [default: %(default)s]")

        # Process arguments
        args = parser.parse_args()

        verbose = args.verbose
        if verbose is None:
            verbose = 0

        if verbose > 0:
            print("Verbose mode on")

        if args.type != 'c' and args.type != 'n':
            print(f"The --type/-t argument must be 'c' or 'n', but was '{args.type}")
            sys.exit(1)
            
        # Load CSV files
        trace1(verbose, f"Loading applicants from {args.applicants}")
        applicants_list = pandas.read_csv(args.applicants).to_dict('records')
        
        folder = args.output_dir

        trace1(verbose, f"Loading list_of_emails from {args.list_of_emails}")
        list_of_emails = pandas.read_csv(args.list_of_emails).to_dict('records')
        set_of_emails = {email_address[EMAIL_ADDRESS] for email_address in list_of_emails}
        
        
        trace1(verbose, "Making emails...")
        email_list = []
        for applicant in applicants_list:
            email_addr = applicant[APPLICANT_EMAIL]
            if email_addr in set_of_emails:
                trace1(verbose, f"Output email for {email_addr}")
                email_list.append({EMAIL_ADDRESS: email_addr,
                                   EMAIL_TEXT: make_email_text(applicant, args.type == "c", verbose)})
            
        # Sort by email address
        trace1(verbose, "Sorting emails...")
        list_of_emails = sorted(list_of_emails, key = lambda i: (i[EMAIL_ADDRESS]))

        trace1(verbose, f"Writing output to {args.output_dir}/output.txt ")
        with open(os.path.join(folder, "output.txt"), "w") as text_file:
            for email_rec in email_list:        
                text_file.write(f"Email address: {email_rec[EMAIL_ADDRESS]}\n\n")
                text_file.write(f"Email text:\n\n{email_rec[EMAIL_TEXT]}\n\n\n\n")

        
        return 0
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception as e:
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + repr(e) + "\n")
        traceback.print_exc()
        sys.stderr.write(indent + "  for help use --help")
        return 2

if __name__ == "__main__":
    sys.exit(main())