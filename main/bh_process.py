'''
Created on May 15, 2020

@author: andrewmcgregor
'''
import sys
import math
import pandas
import os
import random
import json
import hashlib

RUN_TYPE_CLINICAL = 'c'
RUN_TYPE_NONCLINICAL = 'n'
RUN_TYPE_ALLIED = 'a'
RUN_TYPE_RSL = 'r'
RUN_TYPES_LIST = [RUN_TYPE_CLINICAL,
                  RUN_TYPE_NONCLINICAL,
                  RUN_TYPE_ALLIED,
                  RUN_TYPE_RSL]

HR_FIRST_NAME="First name"
HR_LAST_NAME="Last name"
HR_EMAIL_ADDRESS="Email address"
HR_EMPLOYEE_ID="Employee number"
HR_ADDRESS="Address1"
HR_CITY="City"
HR_APT="Apartment #"
HR_PROVINCE="Province"
HR_POSTAL_CODE="Postal Code"
HR_CURRENT_POS="Current position number"
HR_CLASSIFICATION="Classification "
HR_PROGRAM="Program"
HR_FTE="Current FTE"
HR_STEP_RATE="Step rate"
HR_STEP_RATE_DATE="Step rate effective date"
HR_SENIORITY_YEARS="Seniority Years"
HR_SENIORITY_YEARS_STR="Seniority Yrs"
HR_SITE="Site"
HR_SITE__RESIDENCE_ST_LOUIS="RÃ©sidence Saint-Louis"
HR_MODIFIED_WORK="Modified work tag?"
HR_LANG="Language assessment QHR"
HR_LANG__UNKNOWN="unknown"
HR_LANG__ZERO="0"
HR_LANG__NAN="nan"
HR_LANG__ENGLISH="English"
HR_LANG__FRENCH="French"
HR_LANG__BILINGUAL="Bilingual"
HR_ROTATION="Current rotation"

APPLICANT_FIRST_NAME="First"
APPLICANT_LAST_NAME="Last"
APPLICANT_LANG_PROFILE="Lang Profile"
APPLICANT_EMAIL="Email"
APPLICANT_PHONE="Phone"

# Skills are True or False
APPLICANT_SKILL_MH="MH"
APPLICANT_SKILL_PCA="PCA"
APPLICANT_SKILL_Porter="Porter"
APPLICANT_SKILL_RPN="RPN"    
APPLICANT_SKILL_RA="RA"
APPLICANT_SKILL_WC="WC"
APPLICANT_SKILL_RN="RN"

# Support (aka non-clinical) skills
APPLICANT_SKILL_FSA="FSA"
APPLICANT_SKILL_HA="HA"

# Allied health skills
APPLICANT_SKILL_DIET="Diet"
APPLICANT_SKILL_OT="OT"
APPLICANT_SKILL_PT="PT"
APPLICANT_SKILL_SW="SW"
APPLICANT_SKILL_SLP="SLP"

APPLICANT_EXT_APPL="Ext Appl"
APPLICANT_SKILL_C="Skill"

# Units are 1 (most preferred) to 17 (least desired)
APPLICANT_UNIT_EB2BD="EB2BD"
APPLICANT_UNIT_EB3="EB3"
APPLICANT_UNIT_EB4="EB4"
APPLICANT_UNIT_EBPal="EBPal"
APPLICANT_UNIT_EB6="EB6"
APPLICANT_UNIT_EBGDH="EBGDH"
APPLICANT_UNIT_EBPDrm="EBPDrm"
APPLICANT_UNIT_EBGaot="EBGaot"
APPLICANT_UNIT_EBRes="EBRes"
APPLICANT_UNIT_SV2N="SV2N"    
APPLICANT_UNIT_SV2S="SV2S"    
APPLICANT_UNIT_SV3N="SV3N"    
APPLICANT_UNIT_SV3S="SV3S"    
APPLICANT_UNIT_SV4N="SV4N"    
APPLICANT_UNIT_SV4S="SV4S"    
APPLICANT_UNIT_SV5N="SV5N"    
APPLICANT_UNIT_SV5S="SV5S"
# This is a special unit.  It is now in the applicant file as the column
# "Mobile positions acceptable".  If TRUE or blank this "column" is set to "1"
# in the quality checking step
APPLICANT_UNIT_MOBILE="MOBILE" 
# Another special unit - everyone matches this unit
APPLICANT_UNIT_AMBULATORY="AMBULATORY"

APPLICANT_UNIT_NC_EBH_EBR="EBH&EBR"
APPLICANT_UNIT_NC_SVh1="SVh1"
APPLICANT_UNIT_NC_EBH="EBH"
APPLICANT_UNIT_NC_EBR="EBR"
APPLICANT_UNIT_NC_SVH2="SVH2"

APPLICANT_UNIT_ALLIED_EBH="EBH"
APPLICANT_UNIT_ALLIED_SVH="SVH"

APPLICANT_UNIT_NOPREF="SiteNoPref"

APPLICANT_TIME_FT="FT"    
APPLICANT_TIME_PT_1="PT.1"    
APPLICANT_TIME_PT_2="PT.2"   
APPLICANT_TIME_PT_3="PT.3"  
APPLICANT_TIME_PT_4="PT.4"    
APPLICANT_TIME_PT_5="PT.5"    
APPLICANT_TIME_PT_6="PT.6"    
APPLICANT_TIME_PT_7="PT.7"    
APPLICANT_TIME_PT_8="PT.8"    
APPLICANT_TIME_PT_9="PT.9"
APPLICANT_TIME_Stat_NoPref="Stat-NoPref"

APPLICANT_ROT_D="D"    
APPLICANT_ROT_E="E"    
APPLICANT_ROT_N="N"    
APPLICANT_ROT_D_E="D-E"    
APPLICANT_ROT_D_N="D-N"    
APPLICANT_ROT_WW="WW"    

APPLICANT_ROT_ONA8="ONA8"    
APPLICANT_ROT_ONA12="ONA12"

# This is the name of the column which indicates if the applicant
# is open to "Replacement" positions (see POSITION_TEMP) which 
# many people are not.  This was not in the initial survey so
# if defaults to blank which means TRUE, but HR staff can 
# manually set it to "FALSE" to indicate that they should not spend 
# time offering a candidate these positions because they are not 
# interested.
APPLICANT_REPLACEMENT_POS_OK="Replacement positions acceptable"
# Built from APPLICANT_REPLACEMENT_POS_OK where blank is True
APPLICANT_REPLACEMENT_C="Replacement Ok"
APPLICANT_MOBILE_POS_OK="Mobile positions acceptable"

POSITION_POSITION_ID="Position Cd"

# Complicated issue - we can have someone with a regular position who is on leave and then someone else
# getting the same position as a temporary position.  To handle this we are switching to using the position
# id with a flag of ":reg" or ":temp" to avoid key clashes.
POSITION_POSITION_ID_W_TMP="Position Cd w Temp"
POSITION_POSITION_DESC="Position Desc"
POSITION_OCCUPATION="Occupation"
POSITION_SKILL_C="Skill" # See APPLICANT_SKILL_ for values (calculated)
POSITION_CCDESC="CCDesc" # 
POSITION_POSCATEGORY="PosCategory" # 
POSITION_SITE="Site"
POSITION_SITE__CORPORATE = "Corporate" # Set in allied health.  Special case in that it matches any site preferences.
POSITION_EMP_GRP="EmpGrp"
POSITION_UNIT_C="Unit" # See APPLICANT_UNIT_ for values
POSITION_SCHED_UNIT="SchedUnit" # See APPLICANT_UNIT_ for values
POSITION_FTE="FTE" # 
POSITION_TIME_C="Time_calc" # See APPLICANT_TIME_ for values (calculated)
POSITION_SHIFT_HRS="ShiftHrs" # Day/Eve...
POSITION_SHIFT_TYPE="ShiftType" # 8 or 12 or other (e.g. 3.75)
POSITION_ROTATION_C="Rotation" # See APPLICANT_ROT_ for values
POSITION_LANG="Languages required"
POSITION_LANG_DESIG="LangDesig"
POSITION_TEMP="TEMP REPLACEMENT"
POSITION_TEMP_C="TEMP"  # Taken from the "TEMP REPLACEMENT" field (TRUE iff "T" in "TEMP REPLACEMENT")
POSITION_NOMINATION="Nomination"
POSITION_NOMINATION_DATE="Effective date"
POSITION_FILLED="Filled"
# Confusingly this is nothing to do with TEMP REPLACEMENT.  This is TRUE iff the word "Replacement"
# is present in the PosCategory field
POSITION_REPLACEMENT_C="Replacement"  

# Set to true if the position record was created artificially 
POSITION_FROM_RESULTS="PositionFromResults"

REJ_POSITION_ID="Position Cd"
REJ_POSITION_ID_W_TMP="Position Cd w Temp"
REJ_DATE="Date"
REJ_EMPOYEE_ID="Employee number"
REJ_EMAIL="Email"
REJ_FIRST_NAME="First"
REJ_LAST_NAME="Last"
REJ_FTE="FTE"
REJ_TEMP="TEMP"
REJ_UNIT="SchedUnit"
REJ_SITE="Site"
REJ_ROTATION="ShiftType"
REJ_OCCUPATION="Skill"
REJ_SCORE="Score" # Used to reject positions with a score worse than this.

REJ_ACTION="Action"  # Action for position (r-rejected, c-closed, a-accepted)
REJ_ACTION__ACCEPTED="Accepted"  # cPosition was accepted
REJ_ACTION__CLOSED="Closed"  # Applicant was closed without anyone taking any job
REJ_ACTION__REJECTED="Rejected"  # Applicant was closed without anyone taking any job
REJ_ACTION__IGNORE_NOM="Ignorenom"  # Ignore a record in the nomination list

OUTPUT_EMPLOYEE_ID="EmployeeId"
OUTPUT_POSITION_ID=POSITION_POSITION_ID

OFFER_EMPLOYEE_ID="EmployeeId"
OFFER_APPLICANT="Applicant"
OFFER_POSITION_LIST="PositionList"
OFFER_POSITION_LIST_POSITION="Position"
OFFER_POSITION_LIST_PREF_SCORE="PrefRanking"
OFFER_POSITION_LIST_FIRST_OFFERED="FirstOffered"
OFFER_POSITION_LIST_SENIORITY_RANK="Seniority Rank"
OFFER_CLEAR_TO_OFFER="ClearToOffer"
OFFER_PREV_ACCEPT="PrevAccept"
OFFER_SENIORITY_RANKING="SeniorityRanking"

OFFER_BLANK = " "
OFFER_APPLICANT_FIELD="Field"  # Titles like "First Name", "Last Name", etc...
OFFER_APPLICANT_VALUE="Value"  # Values for each title 


INCLUDE_SET_EXTERNAL = "external"
INCLUDE_SET_INTERNAL = "internal"
INCLUDE_SET_RSL = "rsl"

LANGUAGE_STRICT="s"
LANGUAGE_FLEXIBLE="f"

# List of fields in an offer
OFFER_POSITION_FIELDS = ["Score", 
                           POSITION_POSITION_DESC,
                           OFFER_POSITION_LIST_SENIORITY_RANK,
                           POSITION_POSITION_ID,
                           POSITION_SITE,
                           POSITION_SCHED_UNIT,
                           POSITION_SKILL_C,
                           POSITION_FTE,
                           POSITION_SHIFT_TYPE,
                           POSITION_SHIFT_HRS,
                           POSITION_TEMP_C,
                           POSITION_POSCATEGORY,
                           "Position Lang",
                           "Applicant Lang",
                           HR_LANG,
                           OFFER_BLANK,
                           OFFER_APPLICANT_FIELD,
                           OFFER_APPLICANT_VALUE,
                           OFFER_BLANK,
                           HR_EMPLOYEE_ID, APPLICANT_FIRST_NAME, APPLICANT_LAST_NAME, APPLICANT_EMAIL]

# The list of applicant fields to output in the offer CSV files
OFFER_APPLICANT_FIELDS=[
    APPLICANT_FIRST_NAME,
    APPLICANT_LAST_NAME,
    APPLICANT_PHONE,
    APPLICANT_EMAIL,
    HR_MODIFIED_WORK,
    HR_CLASSIFICATION,
    HR_PROGRAM,
    HR_SITE,
    HR_FTE,
    HR_SENIORITY_YEARS,
    HR_EMPLOYEE_ID]

CLINICAL_OCCUPATIONS={
    APPLICANT_SKILL_MH,
    APPLICANT_SKILL_PCA,
    APPLICANT_SKILL_Porter,
    APPLICANT_SKILL_RPN,    
    APPLICANT_SKILL_RA,
    APPLICANT_SKILL_WC,
    APPLICANT_SKILL_RN
    }


NONCLINICAL_OCCUPATIONS={APPLICANT_SKILL_FSA, 
                         APPLICANT_SKILL_HA}

ALLIED_OCCUPATIONS={APPLICANT_SKILL_DIET,
                    APPLICANT_SKILL_OT,
                    APPLICANT_SKILL_PT,
                    APPLICANT_SKILL_SW,
                    APPLICANT_SKILL_SLP
                    }

OCCUPATION_LOOKUP_TABLE={RUN_TYPE_CLINICAL: CLINICAL_OCCUPATIONS,
                         RUN_TYPE_NONCLINICAL: NONCLINICAL_OCCUPATIONS,
                         RUN_TYPE_ALLIED: ALLIED_OCCUPATIONS}

MATCHED_POSITION_FIELDS =[POSITION_POSITION_ID_W_TMP,
                          POSITION_SKILL_C,
                          POSITION_SCHED_UNIT,
                          POSITION_POSITION_DESC,
                          POSITION_FTE,
                          POSITION_EMP_GRP,
                          POSITION_SHIFT_TYPE,
                          POSITION_SHIFT_HRS,
                          POSITION_TEMP_C,
                          POSITION_POSCATEGORY,
                          APPLICANT_FIRST_NAME,
                          APPLICANT_LAST_NAME,
                          HR_EMPLOYEE_ID,
                          HR_SENIORITY_YEARS_STR,
                          HR_FTE,   
                          HR_MODIFIED_WORK,                    
                          POSITION_LANG_DESIG,
                          APPLICANT_EXT_APPL,
                          APPLICANT_LANG_PROFILE]

APPLICANT_ROTATION_LIST = [APPLICANT_ROT_D,    
                           APPLICANT_ROT_E,
                           APPLICANT_ROT_N,    
                           APPLICANT_ROT_D_E,    
                           APPLICANT_ROT_D_N,    
                           APPLICANT_ROT_WW]


APPLICANT_FTE_LIST = [APPLICANT_TIME_FT,
                      APPLICANT_TIME_PT_1,   
                      APPLICANT_TIME_PT_2, 
                      APPLICANT_TIME_PT_3,
                      APPLICANT_TIME_PT_4,  
                      APPLICANT_TIME_PT_5, 
                      APPLICANT_TIME_PT_6,
                      APPLICANT_TIME_PT_7,
                      APPLICANT_TIME_PT_8,
                      APPLICANT_TIME_PT_9]

APPLICANT_SHIFT_LENGTH_LIST = [APPLICANT_ROT_ONA8, APPLICANT_ROT_ONA12]

APPLICANT_UNIT_LIST_CLINICAL = [APPLICANT_UNIT_EB2BD,
                                APPLICANT_UNIT_EB3,
                                APPLICANT_UNIT_EB4,
                                APPLICANT_UNIT_EBPal,
                                APPLICANT_UNIT_EB6,
                                APPLICANT_UNIT_EBGDH,
                                APPLICANT_UNIT_EBPDrm,
                                APPLICANT_UNIT_EBGaot,
                                APPLICANT_UNIT_EBRes,
                                APPLICANT_UNIT_SV2N,   
                                APPLICANT_UNIT_SV2S,    
                                APPLICANT_UNIT_SV3N, 
                                APPLICANT_UNIT_SV3S, 
                                APPLICANT_UNIT_SV4N,
                                APPLICANT_UNIT_SV4S, 
                                APPLICANT_UNIT_SV5N,
                                APPLICANT_UNIT_SV5S
                                ]

APPLICANT_UNIT_LIST_NON_CLINICAL = [APPLICANT_UNIT_NC_EBH_EBR,
                                    APPLICANT_UNIT_NC_SVh1,
                                    APPLICANT_UNIT_NC_EBH,
                                    APPLICANT_UNIT_NC_EBR,
                                    APPLICANT_UNIT_NC_SVH2,
                                    ]

APPLICANT_UNIT_LIST_ALLIED = [APPLICANT_UNIT_ALLIED_EBH,
                              APPLICANT_UNIT_ALLIED_SVH]


UNIT_LOOKUP_CLINICAL = {
                   "1-SV2 North Nursing": APPLICANT_UNIT_SV2N,
                   "1-SV2 South Nursing": APPLICANT_UNIT_SV2S,
                   "1-SV2 South NonNsg": APPLICANT_UNIT_SV2S,
                   "1-SV3 North Nursing": APPLICANT_UNIT_SV3N,
                   "1-SV3 South Nursing": APPLICANT_UNIT_SV3S,
                   "1-SV4 North Nursing": APPLICANT_UNIT_SV4N,
                   "1-SV4 South Nursing": APPLICANT_UNIT_SV4S,
                   "1-SV5 North Nursing": APPLICANT_UNIT_SV5N,
                   "1-SV5 South Nursing": APPLICANT_UNIT_SV5S,
                   
                   "1-EBRes Nursing": APPLICANT_UNIT_EBRes,
                   "1-E4R NonNsg": APPLICANT_UNIT_EB4, # Confirmed by Debbie
                   "1-Mobile Team": APPLICANT_UNIT_MOBILE, # Special unit decided by Debbie
                   "1-Day Hospital": APPLICANT_UNIT_AMBULATORY, # Special unit decided by Debbie
                   "Day Hospital": APPLICANT_UNIT_AMBULATORY, # Special unit decided by Debbie
                   
                   "1-E2BD NonNursing": APPLICANT_UNIT_EB2BD,
                   "1-E2BD Nursing": APPLICANT_UNIT_EB2BD,
                   "1-EB3R Nursing": APPLICANT_UNIT_EB3,
                   "E3R Non Nursing": APPLICANT_UNIT_EB3,
                   "1-EB4R Nursing": APPLICANT_UNIT_EB4,
                   "E6R Non Nursing": APPLICANT_UNIT_EB6,
                   "E4R Non Nursing": APPLICANT_UNIT_EB4,
                   "1-EBP5 Nursing": APPLICANT_UNIT_EBPal,
                   "1-EB5P Nursing": APPLICANT_UNIT_EBPal,
                   "1-EB6R Nursing": APPLICANT_UNIT_EB6
                   }

# Note that this takes the "Site" not "SchedUnit" like the dict above
UNIT_LOOKUP_NONCLINICAL = {"Saint Vincent Hospital": APPLICANT_UNIT_NC_SVh1,
                           "Elisabeth-Bruyère": APPLICANT_UNIT_NC_EBH,
                           "Corporate - Bruyère": APPLICANT_UNIT_NC_EBH
                          }

UNIT_LOOKUP_ALLIED = {"Saint-Vincent": APPLICANT_UNIT_ALLIED_SVH,
                      "Saint Vincent": APPLICANT_UNIT_ALLIED_SVH,
                      "Elisabeth-Bruyère": APPLICANT_UNIT_ALLIED_EBH,
                      "Corporate": APPLICANT_UNIT_ALLIED_EBH, # Not used - there is special code to let this match any site
                      "Elisabeth Bruyere": APPLICANT_UNIT_ALLIED_EBH
                     }

UNIT_LOOKUP_DICT_BY_RUN_TYPE = {RUN_TYPE_CLINICAL: UNIT_LOOKUP_CLINICAL,
                                RUN_TYPE_NONCLINICAL: UNIT_LOOKUP_NONCLINICAL,
                                RUN_TYPE_ALLIED: UNIT_LOOKUP_ALLIED}

# GLobal list of tracing statements output so far
TRACE_OUTPUT = []

# Warnings
WARNING_LIST = []


def warn(string):
    WARNING_LIST.append(string)
    
    
def output_warnings(folder):
    if len(WARNING_LIST) > 0:
        # Output warnings to tracing
        trace1(1, "WARNINGS:")
        for item in WARNING_LIST:
            trace1(1, f"WARNING: {item}")
            
        # Create warning file
        with open(os.path.join(folder, "WARNINGS.txt"), 'w') as f:
            for item in WARNING_LIST:
                f.write("%s\n" % item.encode("utf-8"))
        
        
def trace1(verbose, string):
    TRACE_OUTPUT.append(string)
    if verbose >= 1:
        print(string)


def trace2(verbose, string):
    TRACE_OUTPUT.append(string)
    if verbose >= 2:
        print(string)


def float_formatter(s):
    return "{:.2f}".format(float(s))

def phone_formatter(s):
    if '(' in s or "-" in s:
        return s
    try:
        return format(int(s[:-1]), ",").replace(",", "-") + s[-1]
    except:
        return s
    
def load_file_as_df(filename):
    '''
    Fun with encodings!  On Windows we need to read XLS/XLSX files with cp1252 to get
    accents and slavic c (ch sound - Radojević) to not fail the file loading. 
    CSV files on Windows remain problematic.  With cp1252 they load accented
    files successfully, but not the slavic C (which loads but shows as ? which
    causes problems elsewhere).  Saving the CSV file with UTF-8 encoding
    in Excel (Save As... -> Web Options) saves the file as UTF-8 but the
    slavic C is still not correct.
    '''
    if filename.endswith(".csv"):
        return pandas.read_csv(filename, encoding="utf-8")
    elif filename.endswith(".xls") or filename.endswith(".xlsx"):
        return pandas.read_excel(filename, encoding="cp1252") 
    else:
        raise BhException(f"Unknown suffix on filename {filename}")
    

def make_pos_id_w_temp(position_id, temp_flag):
    if temp_flag == "T":
        return position_id + ":temp"
    else:
        return str(position_id) + ":reg"
        
def load_open_and_filled_positions(position_filename, run_type):
    '''
    This method loads two sheets (vacancies and nominations) and merges them into one positions table.
    '''
    open_pos_df = pandas.read_excel(position_filename, encoding="utf-8", sheet_name=0)
    pos_list = open_pos_df.to_dict('records')
    # Delete rows after blank position code - fail if more that 20 rows for each of main and closed position sheets
    row_count = 0
    open_pos_w_temp_set = set()
    for pos in pos_list:
        position_id = pos[POSITION_POSITION_ID]
        if position_id == "" or isinstance(position_id, float):
            break
        row_count += 1
        pos[POSITION_POSITION_ID_W_TMP] = make_pos_id_w_temp(position_id, pos[POSITION_TEMP])
        pos[POSITION_FILLED] = False
        open_pos_w_temp_set.add(pos[POSITION_POSITION_ID_W_TMP])
        
    if len(pos_list) != row_count:
        if len(pos_list) - row_count > 20:
            raise BhException(f"Deleting more that 20 rows from the end of the file.  Too scary!  Row count is {row_count} and len(position_list) is {len(pos_list)}  Aborting...")
        del(pos_list[row_count:])
    

    closed_pos_df = pandas.read_excel(position_filename, encoding="utf-8", sheet_name=1)
    closed_pos_list = closed_pos_df.to_dict('records')
    closed_pos_w_temp_set = set()
    for closed_pos in closed_pos_list:
        closed_pos_id = closed_pos[POSITION_POSITION_ID]
        closed_pos_id_w_temp = make_pos_id_w_temp(closed_pos_id, closed_pos[POSITION_TEMP])
        
        # if blank or nan we have hit the end of the file
        if closed_pos_id == "" or isinstance(closed_pos_id, float):
            break
            
        closed_pos[POSITION_POSITION_ID_W_TMP] = closed_pos_id_w_temp
        closed_pos[POSITION_FILLED] = True

        pos_list.append(closed_pos)
        closed_pos_w_temp_set.add(closed_pos_id_w_temp) 

    return pos_list


def make_name_key(first_name, last_name, email):
    return f"{first_name}:{last_name}:{email}"


def merge_hr_into_applicants(applicants, hr_list, run_type, verbose):
    '''
    Merges the HR info with the application info (from the web site).
    This also assigns a number to "Seniority hours" based on "Seniority Date" (for full time staff)
    '''
    new_list = []
    error_list = []
    hr_dict = {}
    for hr in hr_list:
        hr_dict[make_name_key(hr[HR_FIRST_NAME], hr[HR_LAST_NAME], hr[HR_EMAIL_ADDRESS])] = hr
        
    for applicant in applicants:
        if applicant[APPLICANT_LAST_NAME] == "" or isinstance(applicant[APPLICANT_LAST_NAME], float):
            break
        name_key = make_name_key(applicant[APPLICANT_FIRST_NAME], applicant[APPLICANT_LAST_NAME], applicant[APPLICANT_EMAIL])
        if applicant[APPLICANT_EXT_APPL] == True:
            # It turns out that the output of hash() is random after Python 3.3 (between runs). Who knew!?!
            # Using md5 now
            m = hashlib.md5()
            m.update(name_key.encode())
            d = {HR_EMPLOYEE_ID: "EXTAPP-" + m.hexdigest(), HR_SENIORITY_YEARS: 0, HR_MODIFIED_WORK: 0, HR_LANG: HR_LANG__UNKNOWN}
            new_list.append({**applicant, **d})
                                                  
        else:
            if name_key not in hr_dict:
                error_list.append({APPLICANT_FIRST_NAME: applicant[APPLICANT_FIRST_NAME], 
                                   APPLICANT_LAST_NAME: applicant[APPLICANT_LAST_NAME],
                                   APPLICANT_EMAIL: applicant[APPLICANT_EMAIL]})
            else:
                seniority = hr_dict[name_key][HR_SENIORITY_YEARS]
                if isinstance(seniority, float):
                    if math.isnan(seniority):
                        trace1(verbose, f"Unexpected seniority '{seniority}' for applicant {hr_dict[name_key][HR_EMPLOYEE_ID]} in HR file, setting seniority to 0")
                        hr_dict[name_key][HR_SENIORITY_YEARS] = 0
                elif not isinstance(seniority, int):
                    raise BhException(f"Unexpected seniority '{seniority}' in HR file")

                new_list.append({**applicant, **hr_dict[name_key]})
        
    if len(error_list) != 0:
        for error in error_list:
            print(f"ERROR: Applicant with First Name: '{error[APPLICANT_FIRST_NAME]}' and last name: '{error[APPLICANT_LAST_NAME]}' with email '{error[APPLICANT_EMAIL]}' not found in HR file.")
        raise BhException("Error in HR file")

    if len(applicants) - len(new_list) > 20:
        raise BhException("More that 20 lines deleted at the end of the applicants file.  Please investigate.")
    if len(applicants) - len(new_list) > 0:
        trace1(verbose, f"Deleted {len(applicants) - len(new_list)} records after blank last name record")
    
    for applicant in new_list:
        for job in OCCUPATION_LOOKUP_TABLE[run_type]:
            if applicant[job]:
                applicant[APPLICANT_SKILL_C] = job
                break
    return new_list
             
hr_lang_lookup={HR_LANG__ENGLISH: "E",
                HR_LANG__ZERO: None,
                HR_LANG__FRENCH: "F",
                HR_LANG__BILINGUAL: "B",
                HR_LANG__NAN: None,
                HR_LANG__UNKNOWN: None
                }

def item_in_preferences(item):
    try:
        if item == "":
            return False
        if item == 99:
            return False
        if isinstance(item, str):
            item = int(item)
        if math.isnan(item):
            return False
        return True    
    except:
        raise BhException(f"Invalid value {item}")
    
    
def score_position(applicant, position, language, verbose):
    '''
    Return the score for this applicant for this position or -1 if no match allowed
    '''
    if applicant[position[POSITION_SKILL_C]] == False:
        return -1
    

    if position[POSITION_REPLACEMENT_C] and not applicant[APPLICANT_REPLACEMENT_C]:
        trace1(verbose, f"Applicant {applicant[HR_EMPLOYEE_ID]} rejected for position {position[POSITION_POSITION_ID]} due it being a replacement position - applicant doesn't want replacement pos")
        return -1

    pos_unit = position[POSITION_UNIT_C]
    
    if applicant[APPLICANT_UNIT_NOPREF] == True or position[POSITION_SITE] == POSITION_SITE__CORPORATE:
        score_for_unit = 1
    else:
        if not item_in_preferences(applicant[pos_unit]):
            trace1(verbose, f"Applicant {applicant[HR_EMPLOYEE_ID]} rejected for position {position[POSITION_POSITION_ID]} due to unit")
            return -1
        score_for_unit = int(applicant[pos_unit])
    
    if applicant[APPLICANT_TIME_Stat_NoPref] == True:
        score_for_time = 1
    else:
        pos_time = position[POSITION_TIME_C]
        if not item_in_preferences(applicant[pos_time]):
            trace1(verbose, f"Applicant {applicant[HR_EMPLOYEE_ID]} rejected for position {position[POSITION_POSITION_ID]} due to job time")
            return -1
        score_for_time = int(applicant[pos_time])
    
    pos_rot = position[POSITION_ROTATION_C]
    if not item_in_preferences(applicant[pos_rot]):
        trace1(verbose, f"Applicant {applicant[HR_EMPLOYEE_ID]} rejected for position {position[POSITION_POSITION_ID]} due to rotation")
        return -1
    score_for_rot = int(applicant[pos_rot])
        
    # Check the shift length for RNs
    if position[POSITION_SKILL_C] == APPLICANT_SKILL_RN:
        shift_length_score = 1000
        if position[POSITION_SHIFT_HRS] == 8:
            if item_in_preferences(applicant[APPLICANT_ROT_ONA8]):
                shift_length_score = int(applicant[APPLICANT_ROT_ONA8])
            else:
                trace2(verbose, f"Applicant {applicant[HR_EMPLOYEE_ID]} rejected for position {position[POSITION_POSITION_ID]} due to shift length")
                return -1

        elif position[POSITION_SHIFT_HRS] == 12:
            if item_in_preferences(applicant[APPLICANT_ROT_ONA12]):
                shift_length_score = int(applicant[APPLICANT_ROT_ONA12])
            else:
                trace2(verbose, f"Applicant {applicant[HR_EMPLOYEE_ID]} rejected for position {position[POSITION_POSITION_ID]} due to shift length")
                return -1
        else:
            raise BhException(f"Unexpected shift length of {position[POSITION_SHIFT_HRS]} in position {position[POSITION_POSITION_ID]}")  
    else:
        shift_length_score = 1
    
    # If the applicant is bilingual they get no penalty.  If the are E or F and the position requires the other language or bilingual
    # then they get a extra 1000 added to their score.  This is a signal to the recruiters that the applicant is not completely suitable 
    # for the position.
    language_boost = 0
    pos_lang = position[POSITION_LANG]
    appl_lang = hr_lang_lookup[str(applicant[HR_LANG])]
    if appl_lang is None:
        appl_lang = applicant[APPLICANT_LANG_PROFILE]
    if appl_lang != "B":
        if pos_lang != appl_lang:
            if language == LANGUAGE_STRICT:
                trace1(verbose, f"Applicant {applicant[HR_EMPLOYEE_ID]} rejected for position {position[POSITION_POSITION_ID]} due to language mismatch")
                return -1
            else:
                language_boost = 1000
            
    score =  score_for_unit + score_for_time + score_for_rot + shift_length_score + language_boost
    trace1(verbose, f"Applicant {applicant[HR_EMPLOYEE_ID]} got score {score} for position {position[POSITION_POSITION_ID]} ")
    return score


def lookup(value, lookup_table):
    if isinstance(value, str):
        value = value.strip()
    x = lookup_table.get(value)
    if x is None:
        raise BhException(f"Value '{value}' not found in lookup table")
    return x
                

def round_up_to_nearest_tenth(v):
    v2 = v * 10.0 + 0.99
    v_int = int(v2)
    return v_int / 10.0



def check_for_duplicate_positions(position_list, verbose):
    position_id_set_w_temp = set()
    for position in position_list:
        position_id_w_temp = position[POSITION_POSITION_ID_W_TMP]
        
        # Check for duplicate id
        if position_id_w_temp in position_id_set_w_temp:
            raise BhException(f"ERROR: position id {position_id_w_temp} is duplicated in the position file")
        position_id_set_w_temp.add(position_id_w_temp)
        

def position_data_quality_check(position_list, verbose):
    '''
    Check if position id is duplicated and create "_calc" fields based on other fields
    '''
    skill_lookup = {"FSA": APPLICANT_SKILL_FSA,
                    "HA": APPLICANT_SKILL_HA,
                    "RN": APPLICANT_SKILL_RN,
                    "RPN": APPLICANT_SKILL_RPN,
                    "PCA": APPLICANT_SKILL_PCA,
                    "WC": APPLICANT_SKILL_WC,
                    "RA": APPLICANT_SKILL_RA,
                    "PORT": APPLICANT_SKILL_Porter,
                    "REHA": APPLICANT_SKILL_RA,
                    "MH": APPLICANT_SKILL_MH,
                    "DT": APPLICANT_SKILL_DIET,
                    "OT": APPLICANT_SKILL_OT,
                    "PT": APPLICANT_SKILL_PT,
                    "MSW": APPLICANT_SKILL_SW,
                    "SLP": APPLICANT_SKILL_SLP
                    }

    shift_lookup = {"Day": APPLICANT_ROT_D,
                    "Day Evening": APPLICANT_ROT_D_E,
                    "Day/Evening": APPLICANT_ROT_D_E,
                    "Day Night": APPLICANT_ROT_D_N,
                    "Night": APPLICANT_ROT_N,
                    "Evening Night": APPLICANT_ROT_N, # ??
                    "Night Evening": APPLICANT_ROT_N, # ??
                    "Evening": APPLICANT_ROT_E}
    time_lookup = {1.0: "FT",
                    0.9: "PT.9",
                    0.8: "PT.8",
                    0.7: "PT.7",
                    0.6: "PT.6",
                    0.5: "PT.5",
                    0.4: "PT.4",
                    0.3: "PT.3",
                    0.2: "PT.2",
                    0.1: "PT.1"}

    clinical_language_lookup = {"": "E",
                                "Bilingualism not req": "E",
                                "Bilingualism not required": "E",
                                "Bilingual": "B"}
    
    row_count = 0
    for position in position_list:
        position_id_w_temp = position[POSITION_POSITION_ID_W_TMP]
                
        position[POSITION_TEMP_C] = position[POSITION_TEMP]
            
        # A position is a "Replacement" position if the word "Replacement" is in the PosCategory (e.g. "WW Replacement")
        position[POSITION_REPLACEMENT_C] = "Replacement" in position[POSITION_POSCATEGORY]
            
        # Make computed fields
        position[POSITION_SKILL_C] = lookup(position[POSITION_OCCUPATION], skill_lookup) 
        # Check if occupation is known
        skill = position[POSITION_SKILL_C]
        found = False
        for occ_list in OCCUPATION_LOOKUP_TABLE.values():
            if skill in occ_list:
                found = True
                
        if not found:
            raise BhException(f"ERROR: position id {position_id_w_temp} has occupation {skill} which is not in the occupation lists")
        
        position[POSITION_ROTATION_C] = lookup(position[POSITION_SHIFT_TYPE], shift_lookup) 
        position[POSITION_TIME_C] = lookup(round_up_to_nearest_tenth(position[POSITION_FTE]), time_lookup) 
        if isinstance(position[POSITION_SHIFT_HRS], str):
            try:
                position[POSITION_SHIFT_HRS] = float(position[POSITION_SHIFT_HRS])
            except:  # Ignore failure let there be a problem later if this is non-numeric for an RN job
                if "12" in position[POSITION_SHIFT_HRS]:  # Cover 12/8 scenario
                    position[POSITION_SHIFT_HRS] = 12.0
            
        if skill in CLINICAL_OCCUPATIONS:
            position[POSITION_UNIT_C] = lookup(position[POSITION_SCHED_UNIT], UNIT_LOOKUP_CLINICAL) 
            lang_desig = position[POSITION_LANG_DESIG]
            if isinstance(lang_desig, float) and math.isnan(lang_desig):
                lang_desig = ""
            position[POSITION_LANG] = lookup(lang_desig, clinical_language_lookup)
        elif skill in ALLIED_OCCUPATIONS:
            position[POSITION_UNIT_C] = lookup(position[POSITION_SITE], UNIT_LOOKUP_ALLIED) 
            lang_desig = position[POSITION_LANG_DESIG]
            position[POSITION_LANG] = lookup(lang_desig, clinical_language_lookup)
        else:
            # Non clinical  positions
            position[POSITION_UNIT_C] = lookup(position[POSITION_SITE], UNIT_LOOKUP_NONCLINICAL) 
            if position[POSITION_LANG_DESIG] == "Bilingual":
                position[POSITION_LANG] = "B"
            else:
                if position[POSITION_SITE] == "Saint Vincent Hospital":
                    position[POSITION_LANG] = "E"
                elif position[POSITION_SITE] == "Elisabeth-Bruyère" or position[POSITION_SITE] == "Corporate - Bruyère":
                    position[POSITION_LANG] = "F"
                else:
                    raise BhException(f"ERROR: position id {position_id_w_temp} is has an unknown site '{position[POSITION_SITE]}'")
        row_count += 1
        
    
class BhException(Exception):
    pass


def applicant_data_quality_check(applicant_list, run_type, verbose):
    # Check if employee id is duplicated
    duplicate_employee_name_dict = {}
    employee_id_set = set()
    for employee in applicant_list:
        employee_id = employee[HR_EMPLOYEE_ID]

        # Set up Mobile
        mobile_ok = employee[APPLICANT_MOBILE_POS_OK]
        if math.isclose(mobile_ok, 0):
            employee[APPLICANT_UNIT_MOBILE] = 99
        elif math.isclose(mobile_ok, 1):
            employee[APPLICANT_UNIT_MOBILE] = 1
        else: # If not true or false (esp. "" or nan) set to acceptable
            employee[APPLICANT_UNIT_MOBILE] = 1
        
        # Set up replacement
        replacement_ok = employee[APPLICANT_REPLACEMENT_POS_OK]
        if math.isclose(replacement_ok, 0):
            employee[APPLICANT_REPLACEMENT_C] = False
        elif math.isclose(replacement_ok, 1):
            employee[APPLICANT_REPLACEMENT_C] = True
        else: # If not true or false (esp. "" or nan) set to acceptable
            employee[APPLICANT_REPLACEMENT_C] = True
        
        # Ambulatory  is a special unit which was not in the IS applicant web site so everybody gets it
        employee[APPLICANT_UNIT_AMBULATORY] = 1
        if employee_id in employee_id_set:
            duplicate_employee_name_dict[employee_id] = {APPLICANT_FIRST_NAME: employee[APPLICANT_FIRST_NAME], APPLICANT_LAST_NAME: employee[APPLICANT_LAST_NAME]}
        employee_id_set.add(employee_id)
        
        no_rotatation_set = True
        # Allied applicants have no rotation - let them "like" all rotation
        if run_type != RUN_TYPE_ALLIED:
            for rot in APPLICANT_ROTATION_LIST:
                if employee[rot] != "" and not math.isnan(employee[rot]):
                    no_rotatation_set = False
        # If no rotation preferences set, then all rotations are considered equally
        if no_rotatation_set:
            trace1(verbose, f"For employee {employee_id} setting all rotations acceptable")
            for rot in APPLICANT_ROTATION_LIST:
                employee[rot] = 1
    
    if len(duplicate_employee_name_dict) > 0:
        raise BhException(f"Duplicate records in applicant list: {json.dumps(duplicate_employee_name_dict, indent=4, sort_keys=True)}")
    
def find_applicant(applicant_list, first_name, last_name, email):
    for applicant in applicant_list:
        if applicant[APPLICANT_FIRST_NAME] == first_name and applicant[APPLICANT_LAST_NAME] == last_name and applicant[APPLICANT_EMAIL] == email:
            return applicant
    return None


def find_position(position_list, positionId, fte, shiftType):
    position_found = None
    for position in position_list:
        if position[POSITION_POSITION_ID] == positionId:
            position_found = position
            break
    
    # Position has been deleted from the position file because it was accepted.  Not a
    # problem because we keep old positions in the results file.
    if position_found == None:
        return None
    
    if not math.isclose(position_found[POSITION_FTE], fte) or position_found[POSITION_SHIFT_TYPE] != shiftType:
        raise BhException(f"Result with position '{position_found[POSITION_POSITION_ID]}' did not match FTE ({position_found[POSITION_FTE]} in position file, {fte} in result file) and shift type ({ position_found[POSITION_SHIFT_TYPE]} in position file, {shiftType} in results file) in the position file")
            
    return position_found


def temp_flag_match(temp_flag1, temp_flag2):
    # Studpidly nan != nan so if they are both float, I assume they are both nan
    if isinstance(temp_flag1, float) and isinstance(temp_flag2, float):
        return True
    
    return temp_flag1 == temp_flag2


def find_pos_by_id_and_date(pos_list, pos_id, temp_flag, nomination_date):
    pos_subset = [pos for pos in pos_list if pos[POSITION_POSITION_ID] == pos_id  and temp_flag_match(pos[POSITION_TEMP], temp_flag) and pos[POSITION_FILLED] and nomination_date == pos.get(POSITION_NOMINATION_DATE)]
    if len(pos_subset) == 0:
        for pos in pos_list:
            if pos[POSITION_POSITION_ID] == pos_id:
                trace1(2, f"Found pos {pos[POSITION_POSITION_ID]} temp_flag {pos[POSITION_TEMP]} filled {pos[POSITION_FILLED]} date {pos.get(POSITION_NOMINATION_DATE)}")
        raise BhException(f"No nomination position record found for {pos_id} with temp flag '{temp_flag}' and nomination date {nomination_date}")
    return pos_subset[0]


def result_data_quality_check(result_list, applicant_list, position_list):
    # Delete trailing lines
    if len(result_list) > 0:
        while isinstance(result_list[-1][REJ_ACTION], float):
            del result_list[-1]
    
    # Check if names are present in applicant list
    for result in result_list:
        
        position_id = result[REJ_POSITION_ID]
        action = result[REJ_ACTION]
        if action not in [REJ_ACTION__ACCEPTED, REJ_ACTION__CLOSED, REJ_ACTION__REJECTED, REJ_ACTION__IGNORE_NOM]:
            raise BhException(f"Unexpected action '{action}' found")
        
        if isinstance(position_id, float) and math.isnan(position_id):
            result[REJ_POSITION_ID_W_TMP] = None
        else:
            result[REJ_POSITION_ID_W_TMP] = make_pos_id_w_temp(position_id, result[REJ_TEMP])

        if action == REJ_ACTION__IGNORE_NOM:
            pos = find_pos_by_id_and_date(position_list, result[REJ_POSITION_ID], result[REJ_TEMP], result[REJ_DATE])
            position_list.remove(pos)
            trace1(2, f"Ignoring nomination record for position {result[REJ_POSITION_ID_W_TMP]} on date {result[REJ_DATE]} due to Ignorenom record")


        if pandas.isna(result[REJ_DATE]):
            result[REJ_DATE] = pandas.Timestamp("2020-01-31")
        applicant = find_applicant(applicant_list, result[REJ_FIRST_NAME], result[REJ_LAST_NAME], result[REJ_EMAIL])
        if applicant is None:
            if result[REJ_ACTION] not in [REJ_ACTION__CLOSED, REJ_ACTION__IGNORE_NOM]:
                raise BhException(f"Result with applicant '{result[REJ_FIRST_NAME]}' '{result[REJ_LAST_NAME]}' from the result file not found in applicant file")
        else:
            if applicant[HR_EMPLOYEE_ID] != result[REJ_EMPOYEE_ID]:
                raise BhException(f"Result with {result[REJ_EMPOYEE_ID]} does not match the employee id from the HR file ({applicant[HR_EMPLOYEE_ID]})")
        find_position(position_list, result[REJ_POSITION_ID], result[REJ_FTE], result[REJ_ROTATION])
        
    # If there are two accepts for the same job , delete the older one by date.  If the dates are identical flag an error
    dict_by_employee_of_list_of_accept_records = {}
    for result in result_list:
        if result[REJ_ACTION] == REJ_ACTION__ACCEPTED:
            employee_id = result[REJ_EMPOYEE_ID]
            if employee_id not in dict_by_employee_of_list_of_accept_records:
                dict_by_employee_of_list_of_accept_records[employee_id] = []
            dict_by_employee_of_list_of_accept_records[employee_id].append(result)
    
    for employee_id in dict_by_employee_of_list_of_accept_records.keys():
        lst = dict_by_employee_of_list_of_accept_records[employee_id]
        if len(lst) > 1:
            # Sort list by date
            lst = sorted(lst, key = lambda i: (i[REJ_DATE]))
            
            # Check if adjacent records have the same date
            for i in range(0, len(lst) - 1):
                if lst[i][REJ_DATE] == lst[i+1][REJ_DATE]:
                    raise BhException(f"Two acceptance records for employee {employee_id} (name {lst[i][REJ_LAST_NAME]})have the same date({lst[i][REJ_DATE]}).  Please fix this.")
            
            # Now delete all result records except the last one in the list.  But if the older record is permanent and the newer record is 
            # temporary then leave both records in place because this is OK (and the permanent position will be reposted as temporary so
            # that it can be filled.
            last_pos = lst[len(lst)-1]
            last_is_temp = last_pos[REJ_TEMP] == "T"
            for i in range(0, len(lst)-1):
                result = lst[i]
                if last_is_temp and i == len(lst) - 2 and result[REJ_TEMP] != "T":                   
                    trace1(2, f"Left in the older accept record for employee {employee_id} with date {result[REJ_DATE]} because it is perm and the last position accepted is temporary")
                else:
                    trace1(2, f"Removed accept record for employee {employee_id} with date {result[REJ_DATE]}")
                    result_list.remove(result)
    
    
    
def was_rejected(rej_dict, applicant, position):
    if applicant[HR_EMPLOYEE_ID] in rej_dict:
        if position[POSITION_POSITION_ID] in rej_dict[applicant[HR_EMPLOYEE_ID]]:
            return True
    return False


def make_matched_positions_list(offer_list, position_list, new_applicants_list):
    '''
    Make a list (for the union) of positions with each applicant for each position.
    The list is sorted by position, then by seniority of each employee.  
    '''
    new_list = []
    for output_rec in offer_list:
        applicant = output_rec[OFFER_APPLICANT]
        applicant[HR_SENIORITY_YEARS_STR] = '{0:.2f}'.format(applicant[HR_SENIORITY_YEARS])

        new_pos_list = [{"Score": pos_rec[OFFER_POSITION_LIST_PREF_SCORE], 
                         **pos_rec[OFFER_POSITION_LIST_POSITION],
                         **applicant}
                         for pos_rec in output_rec[OFFER_POSITION_LIST]]
        new_list += new_pos_list
    return new_list

    
def add_blank_lines_and_sort_matched_list(new_list):
    out_list = [l for l in new_list]
    # Add list of blank records to separate the positions (once sorted)
    pos_id_set = set()
    for item in new_list:
        pos_id_set.add(item[POSITION_POSITION_ID])
    out_list += [{POSITION_POSITION_ID: position_id,  HR_SENIORITY_YEARS: 0} for position_id in pos_id_set]

    # Sort by secondary key, then by primary key
    out_list = sorted(out_list, key = lambda i: (i[HR_SENIORITY_YEARS]), reverse=True)
    out_list = sorted(out_list, key = lambda i: (i[POSITION_POSITION_ID]))
    return out_list



def process_positions2(position_list, applicant_list, rejection_list, include_set, language, verbose):
    '''
    Returns a list of records per applicant (in seniority order).  Each record has fields
    described by the OFFER_* constants.  In particular, each applicant has a list of 
    positions that they matched. 
    
    Also returns a list of unmatched positions and applicants
    '''
    
    # Sort the applicants by seniority - randomize first so that all of the 
    random.shuffle(applicant_list)
    sorted_applicants = sorted(applicant_list, key = lambda i: (i[HR_SENIORITY_YEARS]), reverse=True)
    
    # Copy the list because we delete matched jobs - also exclude filled positions
    position_list = [position for position in position_list if not position[POSITION_FILLED]]
    
    closed_position_set_w_temp = set()
    closed_applicant_set = set()
    accepted_applicant_dict = {} # Dict of accepted employees to score - create offers 
    rejected_applicant_dict = {}
    for rejection in rejection_list:
        employeeId = rejection[REJ_EMPOYEE_ID]
        positionId_w_temp = rejection[REJ_POSITION_ID_W_TMP]
        action = rejection[REJ_ACTION] 
        if employeeId != "":
            if action == REJ_ACTION__ACCEPTED:
                accepted_applicant_dict[employeeId] = rejection[REJ_SCORE]
            elif action == REJ_ACTION__CLOSED:
                closed_applicant_set.add(employeeId)
            elif action == REJ_ACTION__IGNORE_NOM:
                pass
            elif action == REJ_ACTION__REJECTED:
                if positionId_w_temp == "":
                    raise BhException(f"Rejection for {employeeId} had an empty position field.  Please correct.")
                if employeeId not in rejected_applicant_dict:
                    rejected_applicant_dict[employeeId] = set()
                rejected_applicant_dict[employeeId].add(positionId_w_temp)
            else:
                raise BhException(f"Unexpected action '{action}' in results file for employee {employeeId}")
                sys.exit(1)
        if positionId_w_temp != "":
            if action == REJ_ACTION__CLOSED or action == REJ_ACTION__ACCEPTED:
                closed_position_set_w_temp.add(positionId_w_temp)
            elif action == REJ_ACTION__REJECTED or action == REJ_ACTION__IGNORE_NOM:
                pass
            else:
                raise BhException(f"Unexpected action '{action}' in results file for employee {employeeId}")
                
    
    # List of dicts.  Each dict contains: applicant record, list of positions in preference order (with "preference ranking" and "first offered" flags), 
    # seniority ranking (1 is most seniority)
    output_list = []
    leftover_applicant_list = []
    offered_position_id_set_w_temp = set()
    offer_count_dict = {}
    
    current_seniority_ranking = 1
    for applicant in sorted_applicants:
        employeeId = applicant[HR_EMPLOYEE_ID]
        if applicant[APPLICANT_EXT_APPL]:
            allowed = INCLUDE_SET_EXTERNAL in include_set
        else:
            if applicant[HR_SITE] == HR_SITE__RESIDENCE_ST_LOUIS:
                allowed = INCLUDE_SET_RSL in include_set
            else:
                allowed = INCLUDE_SET_INTERNAL in include_set
            
        if not allowed:
            trace2(verbose, f"Applicant {employeeId} rejected because they are not in the candidates of the 'include' set ({include_set})")
        elif applicant[HR_EMPLOYEE_ID] in closed_applicant_set:
            trace2(verbose, f"Applicant {employeeId} rejected because they are in the result file")
        else:
            pos_list = []
            output_app = {OFFER_EMPLOYEE_ID: applicant[HR_EMPLOYEE_ID], 
                          OFFER_SENIORITY_RANKING: current_seniority_ranking,
                          OFFER_APPLICANT: applicant}
            for position_index in range(0, len(position_list)):
                position = position_list[position_index]
                positionId_w_temp = position[POSITION_POSITION_ID_W_TMP]
                if positionId_w_temp in closed_position_set_w_temp:
                    trace1(verbose, f"Position {positionId_w_temp} has been closed in the result file")
                elif employeeId in rejected_applicant_dict and positionId_w_temp in rejected_applicant_dict[employeeId]:
                    trace1(verbose, f"Position {positionId_w_temp} has been rejected for employee {employeeId} in the result file")
                else:
                    score = score_position(applicant, position, language, verbose)
                    if score != -1:
                        if employeeId in accepted_applicant_dict and score >= accepted_applicant_dict[employeeId]:
                            trace1(verbose, f"Employee {employeeId} accepted a position with score {accepted_applicant_dict[employeeId]} so is not being offered position {positionId_w_temp} with score {score}")
                        else:        
                            # Track the number of people who have previously been offered this position for output in the offer.
                            # Staff are often interested in knowing how many people are ahead of them for a position
                            if positionId_w_temp not in offer_count_dict:
                                offer_count_dict[positionId_w_temp] = 1
                            else:
                                offer_count_dict[positionId_w_temp] += 1
                            pos_list.append({OFFER_POSITION_LIST_FIRST_OFFERED: positionId_w_temp not in offered_position_id_set_w_temp,
                                             OFFER_POSITION_LIST_POSITION: position,
                                             OFFER_POSITION_LIST_PREF_SCORE: score,
                                             OFFER_POSITION_LIST_SENIORITY_RANK: offer_count_dict[positionId_w_temp]
                                             })
                            offered_position_id_set_w_temp.add(positionId_w_temp)
                    
            if len(pos_list) != 0:
                # If all of the positions for this applicant were the first to be offered to anyone then offers
                # can go out for this user.
                clear_to_offer = all(pos[OFFER_POSITION_LIST_FIRST_OFFERED] for pos in pos_list)
                output_app[OFFER_CLEAR_TO_OFFER] = clear_to_offer
                output_app[OFFER_PREV_ACCEPT] = employeeId in accepted_applicant_dict
                # Append the position list sorted by score, descending
                output_app[OFFER_POSITION_LIST] = sorted(pos_list, key = lambda i: (i[OFFER_POSITION_LIST_SENIORITY_RANK]))

                output_list.append(output_app)
            else:
                if not employeeId in  accepted_applicant_dict:
                    leftover_applicant_list.append(applicant)
                
            current_seniority_ranking += 1
            
    # Make list of positions offered to no-one
    leftover_position_list = []
    for position in position_list:
        if position[POSITION_POSITION_ID_W_TMP] not in offered_position_id_set_w_temp and position[POSITION_POSITION_ID_W_TMP] not in closed_position_set_w_temp:
            leftover_position_list.append(position)
            
    return output_list, leftover_position_list, leftover_applicant_list
