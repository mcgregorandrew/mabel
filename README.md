# README Mabel #

Mabel is a tool to match applicants to posistions during hiring.

### What is does this tool do? ###

Given a set of positions and applicants and results to date the tool can
run and generate a list of job offers for each applicant.

See docs/Mabel Documentation.docx for complete details.

### How do I get set up? ###

In Windows, do the following:
1) Install Python without support for all users (no permission problems)
2) pip install pandas xlrd
3) Install git
4) Set up keys to access bitbucket
5) git clone ...
6) Setup PYTHONPATH to include the mabel/main folder
7) Run mabel by doing python <path to bh_main.ph>

### Who do I talk to? ###

* Talk to mcgregorandrew@yahoo.ca
