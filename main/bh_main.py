#!/usr/local/bin/python2.7
# encoding: utf-8
'''
main.bh_main -- Bruyere hiring program

main.bh_main is the main program of a program designed to match applicants to the available positions.


@author:     Andrew McGregor

@copyright:  2020 Andrew McGregor. All rights reserved.

@license:    None

@contact:    mcgregorandrew@yahoo.ca
@deffield    updated: Updated
'''

import sys
import os 
import pandas

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter
from main.bh_process import merge_hr_into_applicants,\
    applicant_data_quality_check, position_data_quality_check, \
    process_positions2, OFFER_APPLICANT, OFFER_SENIORITY_RANKING,\
    APPLICANT_FIRST_NAME, APPLICANT_LAST_NAME, OFFER_POSITION_LIST,\
    OFFER_POSITION_LIST_PREF_SCORE, OFFER_POSITION_LIST_POSITION,\
    OFFER_CLEAR_TO_OFFER, OFFER_POSITION_FIELDS,\
    CLINICAL_OCCUPATIONS, BhException, NONCLINICAL_OCCUPATIONS,\
    APPLICANT_LANG_PROFILE, \
    POSITION_LANG, lookup,\
    MATCHED_POSITION_FIELDS, TRACE_OUTPUT,\
    POSITION_SKILL_C, INCLUDE_SET_EXTERNAL, INCLUDE_SET_INTERNAL,\
    INCLUDE_SET_RSL, trace1, HR_MODIFIED_WORK, result_data_quality_check,\
    OFFER_APPLICANT_VALUE, OFFER_APPLICANT_FIELD, OFFER_APPLICANT_FIELDS,\
    APPLICANT_PHONE, HR_SENIORITY_YEARS, float_formatter, phone_formatter,\
    LANGUAGE_FLEXIBLE, LANGUAGE_STRICT, OFFER_PREV_ACCEPT, HR_LANG,\
    RUN_TYPES_LIST, make_matched_positions_list, RUN_TYPE_ALLIED,\
    RUN_TYPE_NONCLINICAL, RUN_TYPE_CLINICAL, ALLIED_OCCUPATIONS,\
    item_in_preferences, APPLICANT_FTE_LIST,\
    APPLICANT_ROTATION_LIST, APPLICANT_SHIFT_LENGTH_LIST, APPLICANT_UNIT_NOPREF,\
    APPLICANT_TIME_Stat_NoPref, UNIT_LOOKUP_DICT_BY_RUN_TYPE, APPLICANT_EMAIL,\
    HR_EMPLOYEE_ID, load_file_as_df, APPLICANT_EXT_APPL, APPLICANT_SKILL_RN,\
    add_blank_lines_and_sort_matched_list, load_open_and_filled_positions,\
    OFFER_POSITION_LIST_SENIORITY_RANK, \
    APPLICANT_REPLACEMENT_C, output_warnings, check_for_duplicate_positions
import traceback
import time
import shutil

__all__ = []
__version__ = 0.1
__date__ = '2020-05-15'
__updated__ = '2020-05-15'


class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''
    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg
    def __str__(self):
        return self.msg
    def __unicode__(self):
        return self.msg


LANG_LOOKUP = {"E": "English",
               "F": "French",
               "B": "Bilingual"}


def validate_include(include_string):
    include_sets = set()
    for c in include_string:
        if c == 'e':
            include_sets.add(INCLUDE_SET_EXTERNAL)
        elif c == 'i':
            include_sets.add(INCLUDE_SET_INTERNAL)
        elif c == 'r':
            include_sets.add(INCLUDE_SET_RSL)
        else:
            raise BhException(f"Unexpected character in 'include' string.  Characater is '{c}'  allowed characters are 'i', 'e' and 'r'")
    return include_sets


FORMATTER_DICT={APPLICANT_PHONE: phone_formatter,
                HR_SENIORITY_YEARS: float_formatter}

    
def add_prefs(new_pos_list, applicant, index, list_of_fields, title, no_pref_field):
    if index >= len(new_pos_list):
        new_pos_list.append({})
    new_pos_list[index][OFFER_APPLICANT_FIELD] = title
    if no_pref_field is not None and applicant[no_pref_field]:
        new_pos_list[index][OFFER_APPLICANT_VALUE] = "Any"
    else:        
        items = [unit for unit in list_of_fields if item_in_preferences(applicant[unit])]
        new_pos_list[index][OFFER_APPLICANT_VALUE] = ",".join(items)


def add_applicant_info(new_pos_list, applicant, run_type):
    '''
    Adds the applicant information to the dict of offers in new_pos_list as the
    OFFER_APPLICANT_FIELD and OFFER_APPLICANT_VALUE dict elements
    '''
    
    for i in range(0, len(OFFER_APPLICANT_FIELDS)):
        if i >= len(new_pos_list):
            new_pos_list.append({})
        field = OFFER_APPLICANT_FIELDS[i]
        new_pos_list[i][OFFER_APPLICANT_FIELD] = field
        value = applicant.get(field)
        if field in FORMATTER_DICT:
            value = FORMATTER_DICT[field](str(value))
        new_pos_list[i][OFFER_APPLICANT_VALUE] = value
        
    # Add preferences so they can be easily be sent to people getting offers
    i =  len(OFFER_APPLICANT_FIELDS)
    if i >= len(new_pos_list):
        new_pos_list.append({})
    new_pos_list[i][OFFER_APPLICANT_FIELD] = ""
    new_pos_list[i][OFFER_APPLICANT_VALUE] = ""
        
        
    i += 1
    add_prefs(new_pos_list, applicant, i, UNIT_LOOKUP_DICT_BY_RUN_TYPE[run_type].values(), "Acceptable Units", APPLICANT_UNIT_NOPREF)
    
    i += 1
    add_prefs(new_pos_list, applicant, i, APPLICANT_FTE_LIST, "Acceptable FTEs", APPLICANT_TIME_Stat_NoPref)
    
    i += 1
    add_prefs(new_pos_list, applicant, i, APPLICANT_ROTATION_LIST, "Acceptable Rotations", None)
    
    i += 1
    if i >= len(new_pos_list):
        new_pos_list.append({})
    new_pos_list[i][OFFER_APPLICANT_FIELD] = "Replacement Acceptable"
    new_pos_list[i][OFFER_APPLICANT_VALUE] = applicant[APPLICANT_REPLACEMENT_C]
    
    if run_type == RUN_TYPE_CLINICAL:
        i += 1
        add_prefs(new_pos_list, applicant, i, APPLICANT_SHIFT_LENGTH_LIST, "Acceptable Shifts (RNs only)", None)
    
    # Add the line to copy into the results file.
    for field in [HR_EMPLOYEE_ID, APPLICANT_FIRST_NAME, APPLICANT_LAST_NAME, APPLICANT_EMAIL]:
        new_pos_list[0][field] = applicant[field]
    

def make_and_save_union_files(matched_positions, folder, run_type, language_setting):
    '''
    We make files of data to share with the unions (CUPE and ONA).  The ONA file needs to include the 
    RNs and the CUPE file needs to include MH, RPN, WC, Porter, FSA, HCA, RA, but only for internal applicants.
    '''
    if language_setting == "s":
        language_tag = "strict language"
    else:
        language_tag = "flexible language"
    date_tag = time.strftime("%y%m%d-%H%M%S")
    ona_name_root = "ONA selection lists"
    ona_positions = [match_rec for match_rec in matched_positions if match_rec[POSITION_SKILL_C] == APPLICANT_SKILL_RN and not match_rec[APPLICANT_EXT_APPL]]
    if len(ona_positions) > 0:
        ona_positions = add_blank_lines_and_sort_matched_list(ona_positions)
        ona_positions_df = pandas.DataFrame(ona_positions, columns=MATCHED_POSITION_FIELDS)
        ona_positions_df.to_excel(os.path.join(folder, f"{ona_name_root} {date_tag} {language_tag}.xlsx"), encoding="utf-8", index=False)

    # Don't do this for allied health people
    if run_type == RUN_TYPE_CLINICAL or run_type == RUN_TYPE_NONCLINICAL:
        if run_type == RUN_TYPE_CLINICAL:
            cupe_name_root = "CUPE selection lists clinical"
        else:
            cupe_name_root = "CUPE selection lists support positions"
            
        cupe_positions = [match_rec for match_rec in matched_positions if match_rec[POSITION_SKILL_C] != APPLICANT_SKILL_RN and not match_rec[APPLICANT_EXT_APPL]]
        if len(cupe_positions) > 0:
            cupe_positions = add_blank_lines_and_sort_matched_list(cupe_positions)
            cupe_positions_df = pandas.DataFrame(cupe_positions, columns=MATCHED_POSITION_FIELDS)
            cupe_positions_df.to_excel(os.path.join(folder, f"{cupe_name_root} {date_tag} {language_tag}.xlsx"), encoding="utf-8", index=False)


    if run_type == RUN_TYPE_ALLIED:
        opseu_name_root = "OPSEU selection lists"
            
        opseu_positions = [match_rec for match_rec in matched_positions if not match_rec[APPLICANT_EXT_APPL]]
        if len(opseu_positions) > 0:
            opseu_positions = add_blank_lines_and_sort_matched_list(opseu_positions)
            opseu_positions_df = pandas.DataFrame(opseu_positions, columns=MATCHED_POSITION_FIELDS)
            opseu_positions_df.to_excel(os.path.join(folder, f"{opseu_name_root} {date_tag} {language_tag}.xlsx"), encoding="utf-8", index=False)


def main(argv=None): # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    program_license = '''%s

  Created by Andrew McGregor on %s.
  Copyright 2020 Andrew McGregor. All rights reserved.

  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc, str(__date__))

    try:
        # Setup argument parser
        parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
        parser.add_argument("-a", "--applicants", help="CSV file of applicants", required=True)
        parser.add_argument("-T", "--top_level_files", help="Store all files in top level folder", action="store_true")
        parser.add_argument("-d", "--data_from_hr", help="CSV file of HR applicant day", required=True)
        parser.add_argument("-t", "--type", help="Type of files, c=clinical, n=non-clinical(FSA/HCA), a=allied health, r=Residence-St-Louis", required=True)
        parser.add_argument("-i", "--include", help="Process applicants of these sets 'i' - internal, 'e' - external, 'r' - RSL applicants (e.g. ir or ier)", required=True)
        parser.add_argument("-p", "--positions", help="CSV file of positions", required=True)
        parser.add_argument("-l", "--language", help="Language matching setting. 's' is strict - mismatches rejected, 'f' is flexible - mismatches are scored 1000 higher", required=True)
        parser.add_argument("-o", "--output_dir", help="Folder to write output files to", required=True)
        parser.add_argument("-r", "--results", help="CSV file of positions rejected or accepted by applicants", required=False)
        parser.add_argument('-V', '--version', action='version', version=program_version_message)
        parser.add_argument("-v", "--verbose", dest="verbose", action="count", help="set verbosity level [default: %(default)s]")

        # Process arguments
        args = parser.parse_args()

        verbose = args.verbose
        if verbose is None:
            verbose = 0

        if verbose > 0:
            print("Verbose mode on")

        include_sets = validate_include(args.include)
        trace1(verbose, f"Data include sets: {include_sets}")
        
        if args.type not in RUN_TYPES_LIST:
            print(f"The --type/-t argument must be in {RUN_TYPES_LIST}, but was '{args.type}")
            sys.exit(1)
            
        if args.language == LANGUAGE_STRICT:
            lang_tag = "langs"
        elif args.language == LANGUAGE_FLEXIBLE:
            lang_tag = "langf"
        else:
            print(f"The --language/-l argument must be '{LANGUAGE_STRICT}' or '{LANGUAGE_FLEXIBLE}', but was '{args.type}")
            sys.exit(1)
            
        # Load CSV files
        trace1(verbose, f"Loading applicants from {args.applicants}")
        applicants = load_file_as_df(args.applicants)
        
        trace1(verbose, f"Loading HR file from {args.data_from_hr}")
        hr = load_file_as_df(args.data_from_hr)
        
        new_applicants_list = merge_hr_into_applicants(applicants.to_dict('records'), hr.to_dict('records'), args.type, verbose)
        
        applicant_data_quality_check(new_applicants_list, args.type, verbose)
        
        trace1(verbose, f"Loading positions from {args.positions}")
        position_list = load_open_and_filled_positions(args.positions, args.type)
        position_data_quality_check(position_list, verbose)  # Generates new fields with names based on existing fields (e.g. Registered Nurse -> RN) 
        
        
        # Filter the position list to only have the positions of the type requested
        if args.type == RUN_TYPE_CLINICAL:
            position_list = [position for position in position_list if position[POSITION_SKILL_C] in CLINICAL_OCCUPATIONS]
            output_type_tag = "clinical"
        elif args.type == RUN_TYPE_NONCLINICAL:
            position_list = [position for position in position_list if position[POSITION_SKILL_C] in NONCLINICAL_OCCUPATIONS]
            output_type_tag = "nonclinical"
        elif args.type == RUN_TYPE_ALLIED:
            position_list = [position for position in position_list if position[POSITION_SKILL_C] in ALLIED_OCCUPATIONS]
            output_type_tag = "allied"
        else:
            raise BhException(f"Unknown 'type' argument {args.type}.  Only valid values are 'c' and 'n'")
            
        if args.top_level_files:
            folder = args.output_dir
        else:
            folder = os.path.join(args.output_dir, output_type_tag + "-" + lang_tag + "-" + time.strftime("%Y%m%d-%H%M%S"))
            os.mkdir(folder)
            input_folder = os.path.join(folder, "inputs")
            os.mkdir(input_folder)
            shutil.copyfile(args.positions, os.path.join(input_folder, os.path.basename(args.positions)))
            shutil.copyfile(args.applicants, os.path.join(input_folder, os.path.basename(args.applicants)))
            shutil.copyfile(args.data_from_hr, os.path.join(input_folder, os.path.basename(args.data_from_hr)))
            if args.results: 
                shutil.copyfile(args.results, os.path.join(input_folder, os.path.basename(args.results)))

        if args.results is not None:
            trace1(verbose, f"Loading rejections from {args.results}")
            results_list = load_file_as_df(args.results).to_dict('records')
            # Note this deletes some lines in the position_list (Ignorenom records)
            result_data_quality_check(results_list, new_applicants_list, position_list)
        else:
            results_list = []
        
        # Check for duplicate positions
        check_for_duplicate_positions(position_list, verbose)
        
        trace1(verbose, "Processing files...")
        output_list, unmatched_positions, unmatched_applicants = process_positions2(position_list, new_applicants_list, results_list, include_sets, args.language, verbose)
        trace1(verbose, f"Writing output to {args.output_dir} ")
            
        for output_rec in output_list:
            applicant = output_rec[OFFER_APPLICANT]
            if output_rec[OFFER_CLEAR_TO_OFFER]:
                clear_to_offer_tag = "-CLEAR-TO-OFFER"
            else:
                clear_to_offer_tag = ""
            if applicant[HR_MODIFIED_WORK] in {"WR", "TR"}:
                modified_work_tag = "-MODIFIED_WORK"
            else:
                modified_work_tag = ""
            sub_folder_set = set()
            for pos_rec in output_rec[OFFER_POSITION_LIST]:
                sub_folder_set.add(pos_rec[OFFER_POSITION_LIST_POSITION][POSITION_SKILL_C])
                sub_folder = os.path.join(folder, pos_rec[OFFER_POSITION_LIST_POSITION][POSITION_SKILL_C])
                if not os.path.exists(sub_folder):
                    os.mkdir(sub_folder)
                    
            new_pos_list = [{"Score": pos_rec[OFFER_POSITION_LIST_PREF_SCORE], 
                             OFFER_POSITION_LIST_SENIORITY_RANK: pos_rec[OFFER_POSITION_LIST_SENIORITY_RANK], 
                             **pos_rec[OFFER_POSITION_LIST_POSITION],
                             "Position Lang": lookup(pos_rec[OFFER_POSITION_LIST_POSITION][POSITION_LANG], LANG_LOOKUP),
                             "Applicant Lang": lookup(applicant[APPLICANT_LANG_PROFILE], LANG_LOOKUP),
                             HR_LANG: applicant[HR_LANG]} 
                            for pos_rec in output_rec[OFFER_POSITION_LIST]]
            trace1(verbose, f"Preparing applicant {applicant[APPLICANT_FIRST_NAME]} {applicant[APPLICANT_LAST_NAME]}")
            add_applicant_info(new_pos_list, applicant, args.type)
            output_df = pandas.DataFrame(new_pos_list, columns=OFFER_POSITION_FIELDS)
            for sub_folder in sub_folder_set:
                if output_rec[OFFER_PREV_ACCEPT]:
                    prev_accept_tag = "-PREV_ACCEPT"
                else:
                    prev_accept_tag = ""
                if len(sub_folder_set) > 1:
                    pos_count_tag = "-MultOcc"
                else:
                    pos_count_tag = "-OneOcc"
                fname = "{:0>3d}".format(output_rec[OFFER_SENIORITY_RANKING]) + "-" + applicant[APPLICANT_LANG_PROFILE] + "-" + applicant[APPLICANT_FIRST_NAME] + "-" + applicant[APPLICANT_LAST_NAME] + clear_to_offer_tag + modified_work_tag + pos_count_tag + prev_accept_tag + ".xlsx"
                trace1(verbose, f"Writing file {os.path.join(folder, sub_folder, fname)}")
                output_df.to_excel(os.path.join(folder, sub_folder, fname), index=False, encoding="utf-8")
        
        unmatched_positions_df = pandas.DataFrame(unmatched_positions)
        unmatched_positions_df.to_excel(os.path.join(folder, "unmatched_positions.xlsx"), encoding="utf-8", index=False)
        
        matched_positions = make_matched_positions_list(output_list, position_list, new_applicants_list)
        matched_positions2 = add_blank_lines_and_sort_matched_list(matched_positions)
        matched_positions_df = pandas.DataFrame(matched_positions2, columns=MATCHED_POSITION_FIELDS)
        matched_positions_df.to_excel(os.path.join(folder, "matched_positions.xlsx"), encoding="utf-8", index=False)
        
        make_and_save_union_files(matched_positions, folder, args.type, args.language)
        
        unmatched_applicants_df = pandas.DataFrame(unmatched_applicants)
        unmatched_applicants_df.to_excel(os.path.join(folder, "unmatched_applicants.xlsx"), encoding="utf-8", index=False)
        
        output_warnings(folder)
        
        with open(os.path.join(folder, "mabel_log.txt"), 'w') as f:
            for item in TRACE_OUTPUT:
                f.write("%s\n" % item.encode("utf-8"))
        
        print(f"Data output to folder {folder}")
        return 0
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception as e:
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + repr(e) + "\n")
        traceback.print_exc()
        sys.stderr.write(indent + "  for help use --help\n\n")
        return 2

if __name__ == "__main__":
    sys.exit(main())