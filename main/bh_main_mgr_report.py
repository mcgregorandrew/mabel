#!/usr/local/bin/python2.7
# encoding: utf-8
'''
main.bh_main_email -- Bruyere program to generate email with preferences to allow users to confirm their preferences.


@author:     Andrew McGregor

@copyright:  2020 Andrew McGregor. All rights reserved.

@license:    None

@contact:    mcgregorandrew@yahoo.ca
@deffield    updated: Updated
'''

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter
from main.bh_process import *  # @UnusedWildImport
import traceback
import time
import shutil

__all__ = []
__version__ = 0.1
__date__ = '2020-05-15'
__updated__ = '2020-05-15'


EMAIL_ADDRESS="Email"
EMAIL_TEXT="EmailText"

class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''
    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg
    def __str__(self):
        return self.msg
    def __unicode__(self):
        return self.msg


def make_prefs(applicant, pref_list, mapper_dict, any_field):
    if any_field is not None and applicant[any_field]:
        return "any"
    
    rec_list = []
    for field in pref_list:
        if not math.isnan(applicant[field]) and applicant[field] != 99:
            rec_list.append({"PREF": applicant[field], "FIELD": mapper_dict[field]})
    # Sort the list
    unit_pref_list = sorted(rec_list, key = lambda i: (i["PREF"]))
    # Output string
    mini_list = [s["FIELD"] for s in unit_pref_list]
    return ','.join(mini_list)

TEMP_CURRENT_PROGRAM="Current Program"


LOCAL_QHR_LANG="QHR Language"
LOCAL_SELF_ASSESSED_LANG="Self Assessed Language"
LOCAL_ACCEPTED="Accepted"
LOCAL_ASSIGNED_ELSEWHERE="Assigned Elsewhere"

MGR_POSITION_REPORT_FIELDS=[POSITION_POSITION_ID,
                   POSITION_SCHED_UNIT,
                   POSITION_OCCUPATION,
                   POSITION_POSITION_DESC,
                   POSITION_FTE,
                   POSITION_ROTATION_C,
                   POSITION_LANG,
                   POSITION_TEMP_C,
                   LOCAL_ACCEPTED,
                   HR_EMPLOYEE_ID,
                   LOCAL_ASSIGNED_ELSEWHERE,
                   APPLICANT_FIRST_NAME,
                   APPLICANT_LAST_NAME,
                   APPLICANT_EXT_APPL,
                   HR_CLASSIFICATION,
                   HR_FTE,
                   HR_ROTATION,
                   TEMP_CURRENT_PROGRAM,
                   LOCAL_QHR_LANG,
                   LOCAL_SELF_ASSESSED_LANG]


MGR_APPLICANT_REPORT_FIELDS=[HR_EMPLOYEE_ID,
                   APPLICANT_FIRST_NAME,
                   APPLICANT_LAST_NAME,
                   APPLICANT_EMAIL,
                   APPLICANT_EXT_APPL,
                   HR_SENIORITY_YEARS_STR,
                   HR_CLASSIFICATION,
                   HR_PROGRAM,
                   HR_FTE,
                   HR_ROTATION,
                   LOCAL_QHR_LANG,
                   LOCAL_SELF_ASSESSED_LANG,
                   LOCAL_ACCEPTED,
                   POSITION_POSITION_ID,
                   POSITION_SCHED_UNIT,
                   POSITION_OCCUPATION,
                   POSITION_POSITION_DESC,
                   POSITION_FTE,
                   POSITION_ROTATION_C,
                   POSITION_LANG,
                   POSITION_TEMP_C]


def make_mgr_position_report_dict(position_list, new_applicants_list, result_list, run_type, verbose):
    '''
    Takes a position list and results list and applicants list and returns a dict:
    { <unit1>: [{<position 1 fields and applicant fields if accepted},
                {<position 2 fields and applicant fields if accepted}],
      <unit2>: ...
    }
    '''
    pos_dict = {}
    accept_dict = {make_pos_id_w_temp(result[REJ_POSITION_ID], result[REJ_TEMP]) : result for result in result_list if result[REJ_ACTION] == REJ_ACTION__ACCEPTED}
    applicant_dict = {applicant[HR_EMPLOYEE_ID] : applicant for applicant in new_applicants_list}
    
    for position in position_list:
        position_id_w_tmp = position[POSITION_POSITION_ID_W_TMP]
        if run_type == RUN_TYPE_CLINICAL:
            if not position.get(POSITION_FROM_RESULTS):
                unit = lookup(position[POSITION_SCHED_UNIT], UNIT_LOOKUP_CLINICAL)
            else:
                unit = position[POSITION_UNIT_C]
        else:
            unit = "ALL"
        if unit not in pos_dict:
            pos_dict[unit] = []
        if position_id_w_tmp in accept_dict:    
            accepting_appl_id = accept_dict[position_id_w_tmp][REJ_EMPOYEE_ID]
            applicant = applicant_dict[accepting_appl_id]
            try:
                appl_lang = applicant[APPLICANT_LANG_PROFILE]
                if applicant[APPLICANT_EXT_APPL]:
                    hr_lang = "external"
                    hr_program = "external"
                else:
                    hr_lang = applicant[HR_LANG]                       
                    hr_program = applicant[HR_PROGRAM]
                
                pos_dict[unit].append({**position, "Accepted": True, LOCAL_QHR_LANG: hr_lang, LOCAL_SELF_ASSESSED_LANG: appl_lang, **applicant, TEMP_CURRENT_PROGRAM: hr_program})
            except Exception as e:
                trace1(verbose, f"Error processing {applicant}")
                raise BhException(e)
        else:
            pos_dict[unit].append({**position, "Accepted": False})
    return pos_dict


def make_mgr_applicant_report_dict(position_list, new_applicants_list, result_list, run_type, verbose):
    '''
    Takes a position list and results list and applicants list and returns a list of applicants with details of a position accepted by the applicant.
    '''
    appl_list = []
    accept_dict = {result[REJ_EMPOYEE_ID] : result for result in result_list if result[REJ_ACTION] == REJ_ACTION__ACCEPTED}
    position_dict = {position[POSITION_POSITION_ID] : position for position in position_list}
    
    for applicant in new_applicants_list:
        employee_id = applicant[HR_EMPLOYEE_ID]
        applicant[HR_SENIORITY_YEARS_STR] = '{0:.2f}'.format(applicant[HR_SENIORITY_YEARS])
        if employee_id not in accept_dict:
            appl_list.append({**applicant, 
                              LOCAL_ACCEPTED: False, 
                              LOCAL_QHR_LANG: applicant[HR_LANG], 
                              LOCAL_SELF_ASSESSED_LANG: applicant[APPLICANT_LANG_PROFILE],
                              LOCAL_ASSIGNED_ELSEWHERE: ""})
        else: # Employee accepted a position
            result = accept_dict[employee_id]
            position = position_dict[result[REJ_POSITION_ID]]
            appl_list.append({**applicant, 
                              LOCAL_ACCEPTED: True, 
                              LOCAL_QHR_LANG: applicant[HR_LANG], 
                              LOCAL_SELF_ASSESSED_LANG: applicant[APPLICANT_LANG_PROFILE],
                              LOCAL_ASSIGNED_ELSEWHERE: "",
                              **position})
                                     
    return appl_list


def add_accepted_positions(position_list, results_list, run_type):
    # Make position list by adding accepted positions from the results file for positions
    # which are no longer in the positions "vacancies" or "nominations" tab.
    position_dict = {position[POSITION_POSITION_ID] : position for position in position_list}
    unit_lookup_dict = UNIT_LOOKUP_DICT_BY_RUN_TYPE[run_type]
    
    # For each accepted result, if the position does not exist, add it
    for result in results_list:
        if result[REJ_ACTION] == REJ_ACTION__ACCEPTED:
            if result[REJ_POSITION_ID] not in position_dict:
                trace2(2, f"Adding accepted position from results file for position {result[REJ_POSITION_ID]}")
                if run_type == RUN_TYPE_CLINICAL:
                    unit = unit_lookup_dict[result[REJ_UNIT]]
                elif run_type == RUN_TYPE_NONCLINICAL:
                    unit = unit_lookup_dict[result[REJ_SITE]]
                elif run_type == RUN_TYPE_ALLIED:
                    unit = unit_lookup_dict[result[REJ_SITE]]
                else:
                    raise BhException(f"Unexpected run type {run_type}")
                position_list.append({POSITION_POSITION_ID: result[REJ_POSITION_ID],
                                      POSITION_POSITION_ID_W_TMP: make_pos_id_w_temp(result[REJ_POSITION_ID], result[REJ_TEMP]),
                                      POSITION_FTE: result[REJ_FTE],
                                      POSITION_TEMP: result[REJ_TEMP],
                                      POSITION_ROTATION_C: result[REJ_ROTATION],
                                      POSITION_UNIT_C: unit,
                                      POSITION_OCCUPATION: result[REJ_OCCUPATION],
                                      POSITION_FROM_RESULTS: True})
    return position_list


def mark_obsolete_acceptances(results_list, pos_dict):
    '''
    Remove acceptance records (and associated position records) for positions which were accepted
    by an applicant who then accepted a temp position. Eventually a temp position will be raised for 
    the original position and all reports will look better (each applicant has one job).
    '''
    # Find list of "obsolete" acceptances
    result_acceptances_to_mark = []
    oldest_acceptance_dict_by_emp_id = {}
    for result in results_list:
        if result[REJ_ACTION] == REJ_ACTION__ACCEPTED:
            emp_id = result[REJ_EMPOYEE_ID]
            if emp_id in oldest_acceptance_dict_by_emp_id:
                other_rec = oldest_acceptance_dict_by_emp_id[emp_id]
                if other_rec[REJ_DATE] > result[REJ_DATE]:
                    result_acceptances_to_mark.append(result)
                else:
                    result_acceptances_to_mark.append(other_rec)
                    oldest_acceptance_dict_by_emp_id[emp_id] = result
            else:
                oldest_acceptance_dict_by_emp_id[emp_id] = result

    pos_dict_by_id_w_tmp = {}
    for unit_of_pos in pos_dict.values():
        for pos in unit_of_pos:
            pos_dict_by_id_w_tmp[pos[POSITION_POSITION_ID_W_TMP]] = pos

    for mark_result in result_acceptances_to_mark:
        pos_id_w_tmp = make_pos_id_w_temp(mark_result[REJ_POSITION_ID], mark_result[REJ_TEMP])
        if pos_id_w_tmp in pos_dict_by_id_w_tmp:
            pos = pos_dict_by_id_w_tmp[pos_id_w_tmp]
            pos[LOCAL_ASSIGNED_ELSEWHERE] = LOCAL_ASSIGNED_ELSEWHERE


def main(argv=None): # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    program_license = '''%s

  Created by Andrew McGregor on %s.
  Copyright 2020 Andrew McGregor. All rights reserved.

  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc, str(__date__))

    try:
        # Setup argument parser
        parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
        parser.add_argument("-a", "--applicants", help="CSV file of applicants with preferences", required=True)
        parser.add_argument("-d", "--data_from_hr", help="CSV file of HR applicant day", required=True)
        parser.add_argument("-o", "--output_dir", help="Folder to write output files to", required=True)
        parser.add_argument("-p", "--positions", help="CSV file of positions", required=True)
        parser.add_argument("-r", "--results", help="CSV file of positions rejected or accepted by applicants", required=True)
        parser.add_argument("-t", "--type", help="Type of files, c=clinical, n=non-clinical(FSA/HCA), a=allied health, r=Residence-St-Louis", required=True)
        parser.add_argument("-T", "--top_level_files", help="Store all files in top level folder", action="store_true")
        parser.add_argument('-V', '--version', action='version', version=program_version_message)
        parser.add_argument("-v", "--verbose", dest="verbose", action="count", help="set verbosity level [default: %(default)s]")

        # Process arguments
        args = parser.parse_args()

        verbose = args.verbose
        if verbose is None:
            verbose = 0

        if verbose > 0:
            print("Verbose mode on")

        if args.type not in RUN_TYPES_LIST:
            print(f"The --type/-t argument must be in {RUN_TYPES_LIST}, but was '{args.type}")
            sys.exit(1)
            
        # Load CSV files
        trace1(verbose, f"Loading applicants from {args.applicants}")
        applicants = load_file_as_df(args.applicants)
        
        trace1(verbose, f"Loading HR file from {args.data_from_hr}")
        hr = load_file_as_df(args.data_from_hr)
        
        new_applicants_list = merge_hr_into_applicants(applicants.to_dict('records'), hr.to_dict('records'), args.type, verbose)
        
        applicant_data_quality_check(new_applicants_list, args.type, verbose)
        
        trace1(verbose, f"Loading positions from {args.positions}")
        position_list = load_open_and_filled_positions(args.positions, args.type)
        
        position_data_quality_check(position_list, verbose)  # Generates new fields with names based on existing fields (e.g. Registered Nurse -> RN) 
        position_list = [position for position in position_list if position[POSITION_SKILL_C] in OCCUPATION_LOOKUP_TABLE[args.type]]
                
        results_list = load_file_as_df(args.results).to_dict('records')
        result_data_quality_check(results_list, new_applicants_list, position_list)

        # Generally positions which are accepted are removed from the positions file.  This steps adds the accepted positions back into 
        # the position list, based on the data in the results file
        position_list = add_accepted_positions(position_list, results_list, args.type)

        # Filter the position list to only have the positions of the type requested
        if args.type == RUN_TYPE_CLINICAL:
            output_type_tag = "clinical"
        elif args.type == RUN_TYPE_NONCLINICAL:
            output_type_tag = "nonclinical"
        elif args.type == RUN_TYPE_ALLIED:
            output_type_tag = "allied"
        else:
            raise BhException(f"Unknown 'type' argument {args.type}.  Only valid values are 'c' and 'n'")
        
        pos_dict = make_mgr_position_report_dict(position_list, new_applicants_list, results_list, args.type, verbose)
        app_report_list = make_mgr_applicant_report_dict(position_list, new_applicants_list, results_list, args.type, verbose)
        
        # If an applicant accepts a position and then accepts another position (typically a temp position) it looks like the
        # applicant has two positions.  This is confusing to managers who will see extra staff who are not available to work for them.
        # To avoid this, for each applicant we mark these people as "Assigned Elsewhere".
        mark_obsolete_acceptances(results_list, pos_dict)
        
        if args.top_level_files:
            folder = args.output_dir
        else:
            folder = os.path.join(args.output_dir, output_type_tag + "-mgr_report-" + time.strftime("%Y%m%d-%H%M%S"))
            os.mkdir(folder)
            input_folder = os.path.join(folder, "inputs")
            os.mkdir(input_folder)
            shutil.copyfile(args.positions, os.path.join(input_folder, os.path.basename(args.positions)))
            shutil.copyfile(args.applicants, os.path.join(input_folder, os.path.basename(args.applicants)))
            shutil.copyfile(args.data_from_hr, os.path.join(input_folder, os.path.basename(args.data_from_hr)))
            shutil.copyfile(args.results, os.path.join(input_folder, os.path.basename(args.results)))

        trace1(verbose, f"Writing output to {folder} ")
        master_pos_list = []
        for key in pos_dict.keys():
            pos_list = pos_dict[key]
            master_pos_list.extend(pos_list)
            # Sort by secondary key, then by primary key
            pos_list = sorted(pos_list, key = lambda i: (i[POSITION_POSITION_ID]))
            pos_list = sorted(pos_list, key = lambda i: (i[POSITION_OCCUPATION]))

            unit_df = pandas.DataFrame(pos_list, columns=MGR_POSITION_REPORT_FIELDS)
            unit_df.to_excel(os.path.join(folder, f"{key}.xlsx"), index=False)
        
        # Write combo file
        master_pos_list = sorted(master_pos_list, key = lambda i: (i[POSITION_POSITION_ID]))
        master_pos_list = sorted(master_pos_list, key = lambda i: (i[POSITION_OCCUPATION]))
        
        master_df = pandas.DataFrame(master_pos_list, columns=MGR_POSITION_REPORT_FIELDS)
        master_df.to_excel(os.path.join(folder, f"master_position_list.xlsx"), index=False)
        
        if args.type == RUN_TYPE_CLINICAL:
            MGR_APPLICANT_REPORT_FIELDS.extend([APPLICANT_SKILL_MH, APPLICANT_SKILL_PCA, APPLICANT_SKILL_Porter, APPLICANT_SKILL_RPN,    
                                                APPLICANT_SKILL_RA, APPLICANT_SKILL_WC, APPLICANT_SKILL_RN])
        elif args.type == RUN_TYPE_NONCLINICAL:
            MGR_APPLICANT_REPORT_FIELDS.extend([APPLICANT_SKILL_FSA, APPLICANT_SKILL_HA])
        elif args.type == RUN_TYPE_ALLIED:
            MGR_APPLICANT_REPORT_FIELDS.extend([APPLICANT_SKILL_DIET, APPLICANT_SKILL_OT, APPLICANT_SKILL_PT, APPLICANT_SKILL_SW,    
                                                APPLICANT_SKILL_SLP])
        app_report_df = pandas.DataFrame(app_report_list, columns=MGR_APPLICANT_REPORT_FIELDS)
        app_report_df.to_excel(os.path.join(folder, f"master_applicant_list.xlsx"), index=False)
        
        with open(os.path.join(folder, "mabel_log.txt"), 'w') as f:
            for item in TRACE_OUTPUT:
                f.write("%s\n" % item)
        

        return 0
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception as e:
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + repr(e) + "\n")
        traceback.print_exc()
        sys.stderr.write(indent + "  for help use --help")
        return 2

if __name__ == "__main__":
    sys.exit(main())