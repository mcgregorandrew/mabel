#!/usr/local/bin/python2.7
# encoding: utf-8
'''
main.bh_main_email -- Bruyere program to generate email with preferences to allow users to confirm their preferences.


@author:     Andrew McGregor

@copyright:  2020 Andrew McGregor. All rights reserved.

@license:    None

@contact:    mcgregorandrew@yahoo.ca
@deffield    updated: Updated
'''

import os 

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter
from main.bh_process import *  # @UnusedWildImport
import traceback
import time
import shutil

__all__ = []
__version__ = 0.1
__date__ = '2020-05-15'
__updated__ = '2020-05-15'


EMAIL_ADDRESS="Email"
EMAIL_TEXT="EmailText"

class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''
    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg
    def __str__(self):
        return self.msg
    def __unicode__(self):
        return self.msg




DICT_TOP_LEVEL_REGULAR = "all"
DICT_TOP_LEVEL_MOBILE = "mobile"
DICT_TOP_LEVEL_REPLACEMENT = "replacement"

STATS_DATA_TYPE_NAME = "Data Type"
STATS_OCCUPATION_NAME = "Occupation"
STATS_ROTATION = "Rotation"

def make_empty_info():
    d = {}
    for rot in APPLICANT_ROTATION_LIST:
        d[rot] = {}
        for fte_count in APPLICANT_FTE_LIST:
            d[rot][fte_count] = 0
    return d


def make_stats_dict(position_list, run_type, verbose):
    d = {DICT_TOP_LEVEL_REGULAR: {},
         DICT_TOP_LEVEL_MOBILE: {},
         DICT_TOP_LEVEL_REPLACEMENT: {},
         }
    occ_set = OCCUPATION_LOOKUP_TABLE[run_type]
    
    for occ in occ_set:
        d[DICT_TOP_LEVEL_REGULAR][occ] = make_empty_info()
        d[DICT_TOP_LEVEL_MOBILE][occ] = make_empty_info()
        d[DICT_TOP_LEVEL_REPLACEMENT][occ] = make_empty_info()
        
        
    for position in position_list:
        skill = position[POSITION_SKILL_C]
        if skill in occ_set:
            rot = position[POSITION_ROTATION_C]
            fte = position[POSITION_TIME_C]
            
            d[DICT_TOP_LEVEL_REGULAR][skill][rot][fte] += 1
            if position[POSITION_REPLACEMENT_C]:
                d[DICT_TOP_LEVEL_REPLACEMENT][skill][rot][fte] += 1
            if position[POSITION_UNIT_C] == APPLICANT_UNIT_MOBILE:
                d[DICT_TOP_LEVEL_MOBILE][skill][rot][fte] += 1
                
            
        
    return d


def append_occ(l, d_for_data_type, data_type, occ):
    l.append([data_type, occ])
    occ_dict = d_for_data_type[occ]
    for rot in APPLICANT_ROTATION_LIST:
        l.append([data_type, occ, rot, *[occ_dict[rot][fte_count] for fte_count in APPLICANT_FTE_LIST]])

def append_sub_output(l, d_for_data_type, data_type, occ_set):
    l.append([data_type])
    for occ in occ_set:
        append_occ(l, d_for_data_type, data_type, occ)


def make_output_list(pos_dict, run_type):
    '''
    Make a list of records which will be converted to a Dataframe and output
    '''
    l = []
    occ_set = OCCUPATION_LOOKUP_TABLE[run_type]

    
    append_sub_output(l, pos_dict[DICT_TOP_LEVEL_REGULAR], DICT_TOP_LEVEL_REGULAR, occ_set)
    append_sub_output(l, pos_dict[DICT_TOP_LEVEL_REPLACEMENT], DICT_TOP_LEVEL_REPLACEMENT, occ_set)
    append_sub_output(l, pos_dict[DICT_TOP_LEVEL_MOBILE], DICT_TOP_LEVEL_MOBILE, occ_set)
    return l


def main(argv=None): # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    program_license = '''%s

  Created by Andrew McGregor on %s.
  Copyright 2020 Andrew McGregor. All rights reserved.

  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc, str(__date__))

    try:
        # Setup argument parser
        parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
        parser.add_argument("-o", "--output_dir", help="Folder to write output files to", required=True)
        parser.add_argument("-p", "--positions", help="CSV file of positions", required=True)
        parser.add_argument("-t", "--type", help="Type of files, c=clinical, n=non-clinical(FSA/HCA), a=allied health, r=Residence-St-Louis", required=True)
        parser.add_argument("-T", "--top_level_files", help="Store all files in top level folder", action="store_true")
        parser.add_argument('-V', '--version', action='version', version=program_version_message)
        parser.add_argument("-v", "--verbose", dest="verbose", action="count", help="set verbosity level [default: %(default)s]")

        # Process arguments
        args = parser.parse_args()

        verbose = args.verbose
        if verbose is None:
            verbose = 0

        if verbose > 0:
            print("Verbose mode on")

        if args.type not in RUN_TYPES_LIST:
            print(f"The --type/-t argument must be in {RUN_TYPES_LIST}, but was '{args.type}")
            sys.exit(1)
            
        # Load position file
        trace1(verbose, f"Loading positions from {args.positions}")
        position_df = load_file_as_df(args.positions)
        position_list = position_df.to_dict('records')

        position_data_quality_check(position_list, verbose)  # Generates new fields with names based on existing fields (e.g. Registered Nurse -> RN) 
        position_list = [position for position in position_list if position[POSITION_SKILL_C] in OCCUPATION_LOOKUP_TABLE[args.type]]
                
        # Filter the position list to only have the positions of the type requested
        if args.type == RUN_TYPE_CLINICAL:
            output_type_tag = "clinical"
        elif args.type == RUN_TYPE_NONCLINICAL:
            output_type_tag = "nonclinical"
        elif args.type == RUN_TYPE_ALLIED:
            output_type_tag = "allied"
        else:
            raise BhException(f"Unknown 'type' argument {args.type}.  Only valid values are 'c' and 'n'")
        
        pos_dict = make_stats_dict(position_list, args.type, verbose)
        
        if args.top_level_files:
            folder = args.output_dir
        else:
            folder = os.path.join(args.output_dir, output_type_tag + "-position_stats-" + time.strftime("%Y%m%d-%H%M%S"))
            os.mkdir(folder)
            input_folder = os.path.join(folder, "inputs")
            os.mkdir(input_folder)
            shutil.copyfile(args.positions, os.path.join(input_folder, os.path.basename(args.positions)))

        trace1(verbose, f"Writing output to {folder} ")
        
        pos_stats_list = make_output_list(pos_dict, args.type)
        pos_stats_df = pandas.DataFrame(pos_stats_list, columns=[STATS_DATA_TYPE_NAME, STATS_OCCUPATION_NAME, STATS_ROTATION, *APPLICANT_FTE_LIST])
        pos_stats_df.to_excel(os.path.join(folder, f"position_stats.xlsx"), index=False)
        
        with open(os.path.join(folder, "mabel_log.txt"), 'w') as f:
            for item in TRACE_OUTPUT:
                f.write("%s\n" % item)
        

        return 0
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception as e:
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + repr(e) + "\n")
        traceback.print_exc()
        sys.stderr.write(indent + "  for help use --help")
        return 2

if __name__ == "__main__":
    sys.exit(main())